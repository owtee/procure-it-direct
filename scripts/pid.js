<script type="text/javascript">
<!--
var BrowserDetect = {
	init: function () {
		this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
		this.version = this.searchVersion(navigator.userAgent)
			|| this.searchVersion(navigator.appVersion)
			|| "an unknown version";
		this.OS = this.searchString(this.dataOS) || "an unknown OS";
	},
	searchString: function (data) {
		for (var i=0;i<data.length;i++)	{
			var dataString = data[i].string;
			var dataProp = data[i].prop;
			this.versionSearchString = data[i].versionSearch || data[i].identity;
			if (dataString) {
				if (dataString.indexOf(data[i].subString) != -1)
					return data[i].identity;
			}
			else if (dataProp)
				return data[i].identity;
		}
	},
	searchVersion: function (dataString) {
		var index = dataString.indexOf(this.versionSearchString);
		if (index == -1) return;
		return parseFloat(dataString.substring(index+this.versionSearchString.length+1));
	},
	dataBrowser: [
		{
			string: navigator.userAgent,
			subString: "Chrome",
			identity: "Chrome"
		},
		{ 	string: navigator.userAgent,
			subString: "OmniWeb",
			versionSearch: "OmniWeb/",
			identity: "OmniWeb"
		},
		{
			string: navigator.vendor,
			subString: "Apple",
			identity: "Safari",
			versionSearch: "Version"
		},
		{
			prop: window.opera,
			identity: "Opera"
		},
		{
			string: navigator.vendor,
			subString: "iCab",
			identity: "iCab"
		},
		{
			string: navigator.vendor,
			subString: "KDE",
			identity: "Konqueror"
		},
		{
			string: navigator.userAgent,
			subString: "Firefox",
			identity: "Firefox"
		},
		{
			string: navigator.vendor,
			subString: "Camino",
			identity: "Camino"
		},
		{		// for newer Netscapes (6+)
			string: navigator.userAgent,
			subString: "Netscape",
			identity: "Netscape"
		},
		{
			string: navigator.userAgent,
			subString: "MSIE",
			identity: "Explorer",
			versionSearch: "MSIE"
		},
		{
			string: navigator.userAgent,
			subString: "Gecko",
			identity: "Mozilla",
			versionSearch: "rv"
		},
		{ 		// for older Netscapes (4-)
			string: navigator.userAgent,
			subString: "Mozilla",
			identity: "Netscape",
			versionSearch: "Mozilla"
		}
	],
	dataOS : [
		{
			string: navigator.platform,
			subString: "Win",
			identity: "Windows"
		},
		{
			string: navigator.platform,
			subString: "Mac",
			identity: "Mac"
		},
		{
			   string: navigator.userAgent,
			   subString: "iPhone",
			   identity: "iPhone/iPod"
	    },
		{
			string: navigator.platform,
			subString: "Linux",
			identity: "Linux"
		}
	]

};

BrowserDetect.init();

function doResize () {
   width  = screen.width;
   height = screen.height;
   var docHeight = 0, myWidth = 0, myHeight = 0;
   if( typeof( window.innerWidth ) == 'number' ) {
     //Non-IE
     myWidth = window.innerWidth;
     myHeight = window.innerHeight;
   } else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
     //IE 6+ in 'standards compliant mode'
     myWidth = document.documentElement.clientWidth;
     myHeight = document.documentElement.clientHeight;
   } else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
     //IE 4 compatible
     myWidth  = document.body.clientWidth;
     myHeight = document.body.clientHeight;
       }

   docHeight = document.body.scrollHeight;

   leftHeight = document.getElementById('logo_wrapper').offsetHeight     +
                document.getElementById('top_menu_wrapper').offsetHeight +
                document.getElementById('left_menu').offsetHeight;

   if (docHeight < myHeight) {
      leftFiller = myHeight -leftHeight -100;
      pageHeight = myHeight - 100;
      if (BrowserDetect.browser == 'Explorer'){leftFiller = (leftFiller - 10);}
      if (BrowserDetect.browser == 'Firefox') {leftFiller = (leftFiller - 20);}
      if (BrowserDetect.browser == 'Safari')  {leftFiller = (leftFiller - 20);}
   }
      else
   {
      leftFiller = docHeight - leftHeight;
      pageHeight = docHeight;
      if (BrowserDetect.browser == 'Explorer'){leftFiller = (leftFiller - 6);}
      if (BrowserDetect.browser == 'Firefox') {leftFiller = (leftFiller - 20);}
      if (BrowserDetect.browser == 'Safari')  {leftFiller = (leftFiller - 20);}
   }

   document.getElementById('left_filler').style.height  = leftFiller   + 'px';
   document.getElementById('body_wrapper').style.height = pageHeight   + 'px';
   document.getElementById('work_wrapper').style.height = pageHeight   + 'px';

}

swfobject.registerObject("FlashID");

/script>