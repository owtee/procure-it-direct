<?php
include ($_SERVER['DOCUMENT_ROOT'].'/incs/db.php');

$template_id = '1';

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US">
<html>
<head>
<?php include ($_SERVER['DOCUMENT_ROOT'].'/incs/page_head_area.php');  ?>
  <link rel="stylesheet" type="text/css" href="pid-1.css" />

  <script src="/scripts/swfobject_modified.js" type="text/javascript"></script>

</style>
</head>
<body  onload="doResize()">
<div id="body_wrapper" style="text-align:center;">
   <div id="work_wrapper" >
      <div id="logo_wrapper"><img src="/images/logo.gif">
      </div>
         <?php include ($_SERVER['DOCUMENT_ROOT'].'/incs/banner_wrapper.php');  ?>
         <?php include ($_SERVER['DOCUMENT_ROOT'].'/incs/top_menu.php');  ?>
      <div id="left_menu">
         <?php include ($_SERVER['DOCUMENT_ROOT'].'/incs/left_menu.php');  ?>
         <?php include ($_SERVER['DOCUMENT_ROOT'].'/incs/left_snippet.php');  ?>
         <div id="left_filler">&nbsp;</div>
      </div>

      <div id="body_holder">

         <?php include ($_SERVER['DOCUMENT_ROOT'].'/incs/flash_banner.php');  ?>

          <div id="horizontal_dots">
             <img src="/images/dotted-line-01.gif">
          </div>

         <?php include ($_SERVER['DOCUMENT_ROOT'].'/incs/body_article.php');  ?>

         <?php include ($_SERVER['DOCUMENT_ROOT'].'/incs/latest_news_snippets.php');  ?>






       </div>
   </div>
</div>
<div id="footer_wrapper" >
   <div id="footer_content">
      <div id="footer_links"><a href="#">Worldwide offices</a> | <a href="#">PID Team</a> | <a href="#">Email</a> | <a href="#">Site map</a> | <a href="#">Privacy Statement</a></div>
      <div id="footer_line"><img src="/images/footer-line.gif"></div>
      <div id="footer_address">London Office, Procure it Direct Ltd, 17 Black Friars Lane<br />St Paul's, London, EC4V 6ER<br />Tel: +44 207 248 1238 Fax: +44 207 329 2446</div>
      <div id="footer_credits">Copyright 2008 - 2009, Procure it Direct&trade;. All rights reserved<br />Designed & hosted by AvanGuardia</div>
   </div>
</div>

<?php include ($_SERVER['DOCUMENT_ROOT'].'/incs/admin_screens.php');  ?>

</body>
</html>
<script type="text/javascript">
<!--
var BrowserDetect = {
	init: function () {
		this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
		this.version = this.searchVersion(navigator.userAgent)
			|| this.searchVersion(navigator.appVersion)
			|| "an unknown version";
		this.OS = this.searchString(this.dataOS) || "an unknown OS";
	},
	searchString: function (data) {
		for (var i=0;i<data.length;i++)	{
			var dataString = data[i].string;
			var dataProp = data[i].prop;
			this.versionSearchString = data[i].versionSearch || data[i].identity;
			if (dataString) {
				if (dataString.indexOf(data[i].subString) != -1)
					return data[i].identity;
			}
			else if (dataProp)
				return data[i].identity;
		}
	},
	searchVersion: function (dataString) {
		var index = dataString.indexOf(this.versionSearchString);
		if (index == -1) return;
		return parseFloat(dataString.substring(index+this.versionSearchString.length+1));
	},
	dataBrowser: [
		{
			string: navigator.userAgent,
			subString: "Chrome",
			identity: "Chrome"
		},
		{ 	string: navigator.userAgent,
			subString: "OmniWeb",
			versionSearch: "OmniWeb/",
			identity: "OmniWeb"
		},
		{
			string: navigator.vendor,
			subString: "Apple",
			identity: "Safari",
			versionSearch: "Version"
		},
		{
			prop: window.opera,
			identity: "Opera"
		},
		{
			string: navigator.vendor,
			subString: "iCab",
			identity: "iCab"
		},
		{
			string: navigator.vendor,
			subString: "KDE",
			identity: "Konqueror"
		},
		{
			string: navigator.userAgent,
			subString: "Firefox",
			identity: "Firefox"
		},
		{
			string: navigator.vendor,
			subString: "Camino",
			identity: "Camino"
		},
		{		// for newer Netscapes (6+)
			string: navigator.userAgent,
			subString: "Netscape",
			identity: "Netscape"
		},
		{
			string: navigator.userAgent,
			subString: "MSIE",
			identity: "Explorer",
			versionSearch: "MSIE"
		},
		{
			string: navigator.userAgent,
			subString: "Gecko",
			identity: "Mozilla",
			versionSearch: "rv"
		},
		{ 		// for older Netscapes (4-)
			string: navigator.userAgent,
			subString: "Mozilla",
			identity: "Netscape",
			versionSearch: "Mozilla"
		}
	],
	dataOS : [
		{
			string: navigator.platform,
			subString: "Win",
			identity: "Windows"
		},
		{
			string: navigator.platform,
			subString: "Mac",
			identity: "Mac"
		},
		{
			   string: navigator.userAgent,
			   subString: "iPhone",
			   identity: "iPhone/iPod"
	    },
		{
			string: navigator.platform,
			subString: "Linux",
			identity: "Linux"
		}
	]

};
BrowserDetect.init();

// -->
</script>


<script type="text/javascript">
function doResize () {
   width  = screen.width;
   height = screen.height;
   var docHeight = 0, myWidth = 0, myHeight = 0;
   if( typeof( window.innerWidth ) == 'number' ) {
     //Non-IE
     myWidth = window.innerWidth;
     myHeight = window.innerHeight;
   } else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
     //IE 6+ in 'standards compliant mode'
     myWidth = document.documentElement.clientWidth;
     myHeight = document.documentElement.clientHeight;
   } else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
     //IE 4 compatible
     myWidth  = document.body.clientWidth;
     myHeight = document.body.clientHeight;
       }


   docHeight = document.body.scrollHeight;
   docHeight = document.getElementById('work_wrapper').offsetHeight;

if (docHeight > myHeight) {
      if (BrowserDetect.browser == 'Explorer'){bodyHeight  = (docHeight  - 0);}
      if (BrowserDetect.browser == 'Firefox') {bodyHeight  = (docHeight  + 82);}
      if (BrowserDetect.browser == 'Safari')  {bodyHeight  = (docHeight  + 4);}
      if (BrowserDetect.browser == 'Chrome')  {bodyHeight  = (docHeight  + 4);}
} else {
      if (BrowserDetect.browser == 'Explorer'){bodyHeight  = (myHeight  - 100);}
      if (BrowserDetect.browser == 'Firefox') {bodyHeight  = (myHeight  - 100);}
      if (BrowserDetect.browser == 'Safari')  {bodyHeight  = (myHeight  - 100);}
      if (BrowserDetect.browser == 'Chrome')  {bodyHeight  = (myHeight  - 100);}
}

   leftHeight = document.getElementById('logo_wrapper').offsetHeight     +
                document.getElementById('top_menu_wrapper').offsetHeight +
                document.getElementById('left_menu').offsetHeight;

   leftFiller = bodyHeight - leftHeight;

      if (BrowserDetect.browser == 'Explorer'){leftFiller = (leftFiller + 80);}
      if (BrowserDetect.browser == 'Firefox') {leftFiller = (leftFiller + 80);}
      if (BrowserDetect.browser == 'Safari')  {leftFiller = (leftFiller + 80);}
      if (BrowserDetect.browser == 'Chrome')  {leftFiller = (leftFiller + 80);}

   document.getElementById('body_wrapper').style.height = bodyHeight   + 'px';
   document.getElementById('work_wrapper').style.height = bodyHeight   + 'px';
   document.getElementById('edit_back').style.height    = (bodyHeight + 100)   + 'px';
   document.getElementById('left_filler').style.height  = leftFiller   + 'px';
   document.getElementById('footer_wrapper').style.display = 'block';

}
//-->
</SCRIPT>

<script type="text/javascript">
<!--
swfobject.registerObject("FlashID");
//-->
</script>
<?php if ($_SESSION['logged_in']) { ?>
   <script type="text/javascript">
   var adminWindow
      function admin_popup(url)
      {
         theURL = url;
         theURL = "/admin/loader.php?url="+url;
        adminWindow =  window.open(theURL, "adminWindow", "toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=0,resizable=0,width=700,height=500");
      }
   </SCRIPT>
<?php } ?>
