<?php
include ($_SERVER['DOCUMENT_ROOT'].'/incs/db.php');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US">
<html>
<head>
  <title>PID Menu Admin</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <link rel="stylesheet" type="text/css" href="/admin/latest_news.css" />
</style>
</head>
<body>
<div id="body_wrapper">
</div>
   <div id="news_items_wrapper">
      <iframe name="news_items_frames" height="490" width="300" frameborder="0" scrolling="required" src="news_items_list.php"></iframe>
   </div>
   <div id="news_details_wrapper">
      <iframe name="news_details_frame" height="490" width="540" frameborder="0" scrolling="required" src="blank.php"></iframe>
   </div>
</body>
</html>
