<?php
include ($_SERVER['DOCUMENT_ROOT'].'/incs/db.php');

      $this_page_id = $_GET['page_id'];
      $template_id  = $_GET['template_id'];

      $article_rec  = mysql_query("select * from article_pages where article_page_id = ".$this_page_id);
      $article_rec2 = mysql_fetch_object($article_rec);
      if (!isset($article_rec2->article_title)){
          $article_title    = 'Article Heading';
          $article_text     = 'This is the text for the article';
          $result  = mysql_query("INSERT INTO article_pages (article_page_id, article_title, article_text) values (".$this_page_id.", '".$article_title."', '".$article_text."')")or die ("Can't create".mysql_error());
          header("Location: http://".$_SERVER['HTTP_HOST'].$_SERVER['URL']);
      }
      else
      {
          $article_title    = $article_rec2->article_title;
          $article_text     = $article_rec2->article_text;
      }

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US">
<head>
  <title>PID Menu Admin</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <link rel="stylesheet" type="text/css" href="/admin/body_article.css" />
</style>
<!-- TinyMCE -->
<script type="text/javascript" src="../jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
	tinyMCE.init({
		// General options
		mode : "textareas",
		theme : "advanced",
		plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount",

		// Theme options
		theme_advanced_buttons1 : "bold,italic,|,justifyleft,justifycenter,justifyright,|,cut,copy,paste,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink",
		theme_advanced_buttons2 : "",
		theme_advanced_buttons3 : "",
		theme_advanced_buttons4 : "",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : false,

		// Example content CSS (should be your site CSS)
		content_css : "/admin/body_article.css",

		// Drop lists for link/image/media/template dialogs
		template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js",

		// Replace values for the template plugin
		template_replace_values : {
			username : "Some User",
			staffid : "991234"
		}
	});
</script>
<!-- /TinyMCE -->
</head>
<body>
<div id="body_wrapper">
</div>
   <div id="edit_body_article_wrapper">
<form action="edit_body_article_process.php?page_id=<?php echo $this_page_id; ?>" name="edit_menu" enctype="multipart/form-data" method="post">
      <div id="article_title_text">Title</div>
      <div id="article_title_input"><input type="text" name="article_title" id="article_title" size="40" maxlength="60" value="<?php echo $article_title;?>"></div>
      <div>
	     <textarea id="article_text" name="article_text" rows="15" cols="80" style="width: 80%;font-size:18px;">
            <?php echo $article_text;?>
         </textarea>
      </div>
<?php if ($template_id == '2') { ?>
      <div id="article_title_text"><br>Side Image<br></div>
      <div id="news_title_input"><input type="file" name="side_image" id="side_image" size="50" maxlength="80" style="font-size:12px;" ></div>
<?php } ?>
	  <input id="article_save_button" type="submit" name="save" value="Submit" />
	</div>
   </div>
</form>
</body>
</html>
