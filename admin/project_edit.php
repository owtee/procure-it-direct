<?php
include ($_SERVER['DOCUMENT_ROOT'].'/incs/db.php');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US">
<html>
<head>
  <title>PID Menu Admin</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <link rel="stylesheet" type="text/css" href="/admin/admin_menu.css" />
</style>
</head>
<body>
<div id="body_wrapper">
</div>
   <div id="menu_edit_lines_wrapper" style="width:360px;">
      <iframe name="display_project_list" height="490" width="360" frameborder="0" scrolling="required" src="project_list.php"></iframe>
   </div>
   <div id="menu_edit_form_wrapper" style="width:480px;">
      <iframe name="edit_project_details" height="490" width="480" frameborder="0" scrolling="required" src="blank.php"></iframe>
   </div>
</body>
</html>
