<?php

include ($_SERVER['DOCUMENT_ROOT'].'/incs/db.php');

$action = $_GET['action'];

   if ($action == 'add') {
      $project_activated     = strtotime("now");
      $this_page_title       = 'Add new project';

      $sqlQuery = "INSERT into projects (project_activated)  values  ('".$project_activated."')";
      $result       = mysql_query($sqlQuery) or die ("Can't insert new project".mysql_error());;

      $sqlQuery     = "select * from projects where project_activated = '".$project_activated."'";
      $result2      = mysql_query($sqlQuery);
      $this_project = mysql_fetch_object($result2);

      $project_id   = $this_project->project_id;
      $action       = 'edit';
   }
   else if ($action == 'edit')
   {
      $project_id       = $_GET['id'];
      $this_page_title  = 'Edit project';
   }
   else if ($action == "delete") {
      $project_id      = $_GET['id'];
      $this_page_title  = 'Delete project';
   }


      $rs = mysql_query("select * from projects where project_id = ".$project_id);
      $rsc = mysql_fetch_object($rs);

      $project_title         = $rsc->project_title;
      $project_snippet       = $rsc->project_snippet;
      $project_main_title    = $rsc->project_main_title;
      $project_text          = $rsc->project_text;
      $project_country       = $rsc->project_country;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US">
<head>
  <title>PID Edit Project Details</title>
  <link rel="stylesheet" type="text/css" href="/admin/admin_menu.css" />
<!-- TinyMCE -->
<script type="text/javascript" src="../jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
	tinyMCE.init({
		// General options
		mode : "textareas",
		theme : "advanced",
		plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount",

		// Theme options
		theme_advanced_buttons1 : "bold,italic,|,justifyleft,justifycenter,justifyright,|,cut,copy,paste,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink",
		theme_advanced_buttons2 : "",
		theme_advanced_buttons3 : "",
		theme_advanced_buttons4 : "",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : false,

		// Example content CSS (should be your site CSS)
		content_css : "/admin/body_article.css",

		// Drop lists for link/image/media/template dialogs
		template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js",

		// Replace values for the template plugin
		template_replace_values : {
			username : "Some User",
			staffid : "991234"
		}
	});
</script>
<!-- /TinyMCE -->
</head>
<body style="background:#E0F8F7;">
<form action="project_edit_item_process.php?action=<?php echo $action;?>&id=<?php echo $project_id;?>" name="edit_project" enctype="multipart/form-data" method="post">
<?php if ($action == 'edit') { ?>
<div id="menu_line_wrapper">
<div id="this_page_title"><?php echo $this_page_title;?></div>
<div id="menu_page_name_text">Country</div>
<div id="menu_page_name_input"><input type="text" name="project_country" id="project_country" size="20" maxlength="30" value="<?php echo $project_country;?>"></div>
<div id="menu_title_text">Project Short Title</div>
<div id="menu_title_input"><input type="text" name="project_title" id="project_title" size="20" maxlength="30" value="<?php echo $project_title;?>"></div>
<div id="menu_page_name_text">Snippet</div>
<div id="menu_page_name_input"><input type="text" name="project_snippet" id="project_snippet" size="50" maxlength="80" value="<?php echo $project_snippet;?>"></div>
<div id="menu_page_title_text">Main Title</div>
<div id="menu_page_title_input"><input type="text" name="project_main_title" id="project_main_title" size="50" maxlength="80" value="<?php echo $project_main_title;?>"></div>
<div id="menu_page_description_text">project_text</div>
      <div>
	     <textarea id="project_text" name="project_text" rows="12" cols="60" >
            <?php echo stripslashes($project_text);?>
         </textarea>
      </div>

      <div id="news_title_text">Attached File</div>
      <div id="news_title_input"><input type="file" name="project_file" id="project_file" size="50" ></div>

      <div id="news_title_text">Small Image</div>
      <div id="news_title_input"><input type="file" name="project_thumb_image" id="project_thumb_image" size="50" maxlength="80" ></div>
      <div id="news_title_text">Main Image</div>
      <div id="news_title_input"><input type="file" name="project_main_image" id="project_main_image" size="50" maxlength="80" ></div>

      <div id="news_title_text">Secondary Image</div>
      <div id="news_title_input"><input type="file" name="secondary_image" id="secondary_image" size="50" ></div>

      <div id="news_title_text">Tertiary Image</div>
      <div id="news_title_input"><input type="file" name="tertiary_image" id="tertiary_image" size="50" ></div>

<div style="margin-top:5px;"><input id="menu_submit_button" type="submit" value="Save"></div>
<div style="margin-top:25px;"><img src="/site_images/<?php echo $rsc->project_thumb_image;?>" ></div>
<div style="margin-top:25px;"><img src="/site_images/<?php echo $rsc->project_main_image;?>" ></div>
<div style="margin-top:25px;"><img src="/site_images/<?php echo $rsc->secondary_image;?>" ></div>
<div style="margin-top:25px;"><img src="/site_images/<?php echo $rsc->tertiary_image;?>" ></div>

<?php } else { ?>
      <div id="news_title_text"><?php echo $rsc->project_main_title;?></div>
      <div id="news_title_text"><?php echo $rsc->project_text;?></div>
	  <div id="news_title_input"><input id="news_save_button" type="submit" name="save" value="Delete" /></div>
<?php } ?>

</form>

</div>
</body>
</html>
