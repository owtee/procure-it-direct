<?php

include ($_SERVER['DOCUMENT_ROOT'].'/incs/db.php');

$action = $_GET['action'];

   if ($action == 'add') {
      $page_id          = 0;
      $page_url         = '';
      $page_name        = '';
      $page_title       = '';
      $menu_title       = '';
      $page_type        = 0;
      $page_keywords    = '';
      $page_description = '';
      $parent           = $_GET['parent'];
      $next_seq         = $_GET['next_seq'];
      $menu_line_id     = 0;
      $this_page_title  = 'Add new menu line and page';
   }
   else
   {
      $menu_line_id     = $_GET['menu_line_id'];

      $rs = mysql_query("select * from menu_lines where menu_line_id = ".$menu_line_id);
      $rsc = mysql_fetch_object($rs);

      $page_id          = $rsc->menu_line_page_id;
      $menu_title       = $rsc->menu_line_title;
      $parent           = $rsc->menu_line_parent;

     if (!$page_id == '') {
        $rs2 = mysql_query("select * from site_pages where page_id = ".$page_id);
        $rsc2 = mysql_fetch_object($rs2);
           $page_url         = $rsc2->page_url;
           $page_name        = $rsc2->page_name;
           $page_title       = $rsc2->page_title;
           $page_type        = $rsc2->page_type;
           $page_keywords    = $rsc2->page_keywords;
           $page_description = $rsc2->page_description;
           $next_seq         = 0;
           $this_page_title  = 'Edit menu line and page details';
     } else {
           $page_url         = '';
           $page_name        = '';
           $page_title       = '';
           $page_type        = 0;
           $page_keywords    = '';
           $page_description = '';
           $action           = 'page_add';
           $next_seq         = 0;
           $this_page_title  = 'Edit menu line and add page details';
     }
   }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US">
<head>
  <title>PID Edit Menu Details</title>
  <link rel="stylesheet" type="text/css" href="/admin/admin_menu.css" />
</head>
<body style="background:#E0F8F7;">
<form action="menu_edit_details_process.php?action=<?php echo $action;?>&menu_line_id=<?php echo $menu_line_id;?>&next_seq=<?php echo $next_seq;?>&parent=<?php echo $parent;?>&page_id=<?php echo $page_id;?>" onSubmit="return checkform()" name="edit_menu" method="post">
<div id="menu_line_wrapper">
<div id="this_page_title"><?php echo $this_page_title;?></div>
<div id="menu_title_text">Menu Title</div>
<div id="menu_title_input"><input type="text" name="menu_title" id="menu_title" size="30" maxlength="60" value="<?php echo $menu_title;?>"></div>
<?php if ($page_type == 0){ ?>
<div id="menu_page_type_text">Page Type</div>
<div id="menu_page_type_select">
   <select name="page_type" id="page_type">
      <option value="0">Choose One</option>
      <option value="1">Main Template</option>
      <option value="2">No Side Menu</option>
      <option value="3">Project Detail</option>
      <option value="4">News</option>
   </select>
</div>
<?php } ?>
<div id="menu_page_name_text">Page Name</div>
<div id="menu_page_name_input"><input type="text" name="page_name" id="page_name" size="30" maxlength="60" value="<?php echo $page_name;?>"></div>
<div id="menu_page_title_text">Page Title</div>
<div id="menu_page_title_input"><input type="text" name="page_title" id="page_title" size="30" maxlength="60" value="<?php echo $page_title;?>"></div>
<div id="menu_page_description_text">Page description</div>
<div id="menu_page_description_textarea">
   <textarea rows="5" cols="60" wrap="physical" name="page_description" id="page_description"><?php echo $page_description;?></textarea>
</div>
<div id="menu_keywords_text">Keywords</div>
<div id="menu_keywords_textarea">
   <textarea rows="5" cols="60" wrap="physical" name="page_keywords" id="page_keywords"><?php echo $page_keywords;?></textarea>
</div>
<div ><input id="menu_submit_button" type="submit" value="Save"></div>

</form>
</div>
</body>
</html>

<script type="text/javascript">
function checkform()
{
    var page_ok = 'yes';
	if (document.getElementById('menu_title').value == '')
	{
		alert('Menu Title not entered');
		page_ok = 'no';
	}
<?php if ($page_type == 0){ ?>

	if (document.getElementById('page_type').value == 0)
	{
		alert('Page Type Not Selected');
		page_ok = 'no';
	}
<?php } ?>
	if (document.getElementById('page_name').value == '')
	{
		alert('Page Name not entered');
		page_ok = 'no';
	}

	if (document.getElementById('page_title').value == '')
	{
		alert('Page Title not entered');
		page_ok = 'no';
	}

	if (document.getElementById('page_description').value == '')
	{
		alert('Page Description not entered');
		page_ok = 'no';
	}

	if (document.getElementById('page_keywords').value == '')
	{
		// something else is wrong
		alert('Page Keywords not entered');
		page_ok = 'no';
	}

    if (page_ok == 'no')
    {
       return false;
    }
    else
    {
       return true;
    }
}
</SCRIPT>