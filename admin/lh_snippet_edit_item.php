<?php
include ($_SERVER['DOCUMENT_ROOT'].'/incs/db.php');

      $action  = $_GET['action'];

      if ($action == "add") {
         $activate        = strtotime("now");
         $sqlQuery        = "INSERT INTO left_side_snippets (snippet_activated) values ('".$activate."')";
         $result          = mysql_query($sqlQuery);
         $sqlQuery        = "SELECT * from left_side_snippets where snippet_activated = '".$activate."'";
         $snippet_rec_id  = mysql_query($sqlQuery);
         $snippet_rec_id2 = mysql_fetch_object($snippet_rec_id);
         $snippet_id      = $snippet_rec_id2->snippet_id;
         $action          = 'edit';
      }

      else  if ($action == "edit") {
         $snippet_id      = $_GET['id'];
      }

      else if ($action == "delete") {
         $snippet_id      = $_GET['id'];
      }

      $rs  = mysql_query("select * from left_side_snippets where snippet_id = ".$snippet_id);
	  $rsc = mysql_fetch_object($rs);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US">
<head>
  <title>PID Menu Admin</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <link rel="stylesheet" type="text/css" href="/admin/latest_news.css" />
</style>
<!-- TinyMCE -->
<script type="text/javascript" src="../jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
	tinyMCE.init({
		// General options
		mode : "textareas",
		theme : "advanced",
		plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount",

		// Theme options
		theme_advanced_buttons1 : "bold,italic,|,justifyleft,justifycenter,justifyright,|,cut,copy,paste,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink",
		theme_advanced_buttons2 : "",
		theme_advanced_buttons3 : "",
		theme_advanced_buttons4 : "",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : false,

		// Example content CSS (should be your site CSS)
		content_css : "/admin/body_article.css",

		// Drop lists for link/image/media/template dialogs
		template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js",

		// Replace values for the template plugin
		template_replace_values : {
			username : "Some User",
			staffid : "991234"
		}
	});
</script>
<!-- /TinyMCE -->
</head>
<body>
   <div id="edit_news_wrapper" style="margin-left:10px;">
<form action="lh_snippet_edit_item_process.php?id=<?php echo $snippet_id; ?>&action=<?php echo $action; ?>" name="snippet_edit_form" method="post" enctype="multipart/form-data">
<?php if ($action == 'edit') { ?>
      <div id="news_title_text">Snippet Title</div>
      <div id="news_title_input"><input type="text" name="snippet_title" id="snippet_title" size="20" value="<?php echo $rsc->snippet_title;?>"></div>
      <div id="news_title_text">Snippet text</div>
      <div id="news_title_input"><input type="text" name="snippet_text" id="snippet_text" size="20" value="<?php echo $rsc->snippet_text;?>"></div>
      <div id="news_title_text">Snippet Symbol</div>
      <div id="news_title_input">
         <select name="snippet_symbol" id="snippet_symbol">
            <option value="0">Choose One</option>
            <option value="1" <?php if ($rsc->snippet_symbol == '&pound;'){echo 'selected';} ?>>&pound;</option>
            <option value="2" <?php if ($rsc->snippet_symbol == '$')      {echo 'selected';} ?>>$</option>
            <option value="3" <?php if ($rsc->snippet_symbol == '&euro;') {echo 'selected';} ?>>&euro;</option>
            <option value="4" <?php if ($rsc->snippet_symbol == '?')      {echo 'selected';} ?>>?</option>
         </select>
      </div>
      <div id="news_title_text">Snippet Article Title</div>
      <div id="news_title_input"><input type="text" name="snippet_article_title" id="snippet_article_title" size="40" maxlength="60" value="<?php echo $rsc->snippet_article_title;?>"></div>

      <div id="news_title_text">Select Banner Image</div>
      <div id="news_title_input"><input type="file" name="banner_image_url" id="banner_image_url" size="50" /></div>
<?php
    if (strlen($rsc->snippet_banner_url) > 0 )
    {
?>
      <div id="news_title_text">Selected image</div>
      <div id="news_title_input"><img src="/site_images/<?php print $rsc->snippet_banner_url; ?>"/></div>
<?php
    }//end if
?>
      <div id="news_title_text">Snippet Article</div>
      <div>
	     <textarea id="snippet_article" name="snippet_article" rows="15" cols="80" >
            <?php echo stripslashes($rsc->snippet_article);?>
         </textarea>
      </div>
	  <div id="news_title_input"><input id="snippet_save_button" type="submit" name="save" value="Save" /></div>
<?php } else { ?>
      <div id="news_title_text"><?php echo $rsc->snippet_title;?></div>
      <div id="news_title_text"><?php echo $rsc->snippet_text;?></div>
	  <div id="news_title_input"><input id="news_save_button" type="submit" name="save" value="Delete" /></div>
<?php } ?>
</form>
</div>
</body>
</html>
