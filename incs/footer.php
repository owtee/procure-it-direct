<div id="footer_wrapper" >
   <div id="footer_content">
      <div id="footer_links"><a href="#">Site map</a> | <a href="#">Privacy Statement</a> | <a href="/Representation_Disclaimer.php?mpid=1&amp;mlid=73">Representation Disclaimer</a></div>
      <div style="float:right;margin:5px 0 0;">
          <a href="https://www.facebook.com/ProcureitDirect?ref=hl" target="_blank"><img src="/images/facebook-variation.png" /></a>
          <a href="http://www.linkedin.com/company/procure-it-direct?trk=top_nav_home" target="_blank"><img src="/images/linkedin-variation.png" /></a>
          <a href="http://www.youtube.com/watch?v=v3bjpgh3HNg" target="_blank"><img src="/images/youtube-variation.png" /></a>
          <a href="https://twitter.com/procureitdirect" target="_blank"><img src="/images/twitter-variation.png" /></a>
      </div>
      <div id="footer_line"><img src="/images/footer-line.gif"></div>
      <div id="footer_address">Prague Office, Karlinske Namesti 6, 186 00,<br />Prague 8, Czech Republic<br />Tel: +420 252 547 924</div>
      <div id="footer_credits">
          Copyright 2014 +, Procure it Direct&trade;. All rights reserved<br />Designed & hosted by <a href=www.seaaitch-consultancy.com" style="color:#ffffff;">SeaAitch Consultancy</a><br/>
          <a href="http://www.copyscape.com/plagiarism.php"><img src="/images/copyspace-banner.gif" alt="Protected by CopySpace. Do not copy."/></a>
      </div>
   </div>
</div>
