<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <title>PID test site</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <link rel="stylesheet" type="text/css" href="pid-1.css" />

  <script src="/scripts/swfobject_modified.js" type="text/javascript"></script>

</head>
<body style="font-size:14px;background:#ffffff;">
   <div id="holding_wrapper" style="width:500px;background:#ffffff;float:left;position:absolute;left:50%;margin-left:-250px;text-align:center;">
      <div id="holding_logo" style="width:500px;float:left;text-align:left;"><img src="/images/logo.gif"></div>
      <div id="holding_inner" style="width:470px;text-align:left;color:#7d7d80;float:left;margin-left:30px;">
<p>PROCURE IT DIRECT are in the process of having their website rebuilt, which will be on-line shortly we apologise for any inconvenience caused please check back soon.</p>
<p>PROCURE it DIRECT was established to facilitate the need for Property / Hotel Developers and End Users to procure and deliver materials directly from source to simplify and achieve cost savings on their often complicated and expensive supply chain. For further information on our services please contact one of our offices:</p>
      </div>
      <div id="holding_address1" style="width:200px;float:left;text-align:left;margin-left:30px;margin-top:15px;">Procure It Direct S.R.O.<br>Budova Lighthouse<br>Jankovcova 1569/2c<br>170 00 Praha 7<br>Czech Republic<br><br>Tel: +420 226 218 511<br>Fax: +420 226 218 510<br></div>
      <div id="holding_address2" style="width:200px;float:left;text-align:left;margin-top:15px;">Procure It Direct Ltd<br>17 Black Friars Lane<br>St Pauls<br>LONDON<br>EC4V 6ER<br><br>Tel: +44 (0) 207 248 1238<br>Fax: +44 (0) 207 329 2446<br></div>
   </div>
</body>
</html>
