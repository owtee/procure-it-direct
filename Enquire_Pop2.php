<?php
$errors = array();
if ( isset($_POST['once']))
{
    if ( !isset($_POST['name']) || strlen($_POST['name']) <= 0 )
    {
        $errors[] = "Please fill in your Name";
    }//end if
    if ( !isset($_POST['phone']) || strlen($_POST['phone']) <= 0 )
    {
        $errors[] = "Please fill in your Phone number";
    }//end elseif
    //check if the email field is filled in...
    if ( !isset($_POST['email']) || strlen($_POST['email']) <= 0 )
    {
        $errors[] = "Please enter your Email address";
    }//end if
    //check to see if the user entered a properly formatted email address...
    elseif ( isset($_POST['email']) && !preg_match("/^[\w\.\-\']+@([\w\-]+\.)*[\w-]{2,100}\.[\w]{2,12}$/",$_POST['email']) )
    {
        $errors[] = "Please enter a correctly formatted Email address";
    }//end elseif
    if ( !isset($_POST['enquiry']) || strlen($_POST['enquiry']) <= 0  )
    {
        $errors[] = "Please fill in your Enquiry";
    }//end if
}//end if
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Welcome to the Materials World of MetPrep Ltd</title>

<!-- META TAGS -->
<meta name="description" content="Welcome to the Materials World of MetPrep Ltd"/>
<meta name="keywords" content="Welcome to the Materials World of MetPrep Ltd"/>

<!-- Stylesheets -->
<link href="/Css/main-styles.css" rel="stylesheet" type="text/css" />

<!-- Favicon -->
<link rel="icon" href="http://www.avanguardia.co.uk/MetPrep/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="http://www.avanguardia.co.uk/MetPrep/favicon.ico" type="image/x-icon"/>

<!-- jQuery lib script -->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>


<!-- ie 5.5 - 6 png fix -->
<script type="text/javascript" src="/jQuery/jquery.pngFix.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $(document).pngFix();
    });
</script>
</head>
<body>
    <div style="align:center">

      <div id="enquiry_form_header">Contact form</div>
<?php
if ( !isset($_POST['once']) || $_POST['once'] == 1 && count($errors) > 0 )
{
?>
        <form method="post" name="contact_form" action="">
      	<div id="enquiry_form">
                <input type="hidden" name="once" value="1"/>
<?php
    if ( count($errors) > 0 )
    {
        echo "                   <p>There were some errors with the information you submitted :-</p>\n";
        echo "                   <ul>\n";
        foreach ( $errors as $error )
        {
            echo "                     <li>$error</li>\n";
        }//end foreach
        echo "                   </ul>\n";
    }//end if
?>
			<div class="enquiry_forms_wrapper" style="margin-top:8px; width:60px;">Name:</div>
			<div class="enquiry_input_wrapper" style="margin-top:4px; width:244px; height:22px;"><input type="text" name="name" size="41" value="<?php if (isset($_POST['name'])) { echo $_POST['name'];}?>" /></div>
    		<div class="enquiry_forms_wrapper" style="margin-top:8px; width:60px;">Telephone:</div>
    		<div class="enquiry_input_wrapper" style="margin-top:4px; width:244px; height:22px;"><input type="text" name="phone" size="41" value="<?php if (isset($_POST['phone'])) { echo $_POST['phone'];}?>" /></div>
        	<div class="enquiry_forms_wrapper" style="margin-top:8px; width:60px;">Email:</div>
        	<div class="enquiry_input_wrapper" style="margin-top:4px; width:244px; height:22px;"><input type="text" name="email" size="41" value="<?php if (isset($_POST['email'])) { echo $_POST['email'];}?>" /></div>
        	<div class="enquiry_forms_wrapper" style="margin-top:8px; width:60px;">Enquiry:</div>
        	<div class="enquiry_input_wrapper" style="margin-top:4px; width:244px; height:120px;"><textarea name="enquiry" cols="29" rows="7"><?php if (isset($_POST['enquiry'])) { echo $_POST['enquiry'];}?></textarea></div>

			<div id="btn_wrapper_r">
            	<div id="btn_end_l_r"></div>
                <div id="btn_nav_middle_r">
                	<div class="btn_text_r"><a href="javascript:document.contact_form.submit();">SUBMIT</a></div>
                </div>
                <div id="btn_nav_end_r_r"></div>
            </div>
            <div id="btn_wrapper_r_right">
            	<div id="btn_end_l_r"></div>
                <div id="btn_nav_middle_r">
                	<div class="btn_text_r"><a href="#" onclick="window.parent.Shadowbox.close();">CANCEL</a></div>
                </div>
                <div id="btn_nav_end_r_r"></div>
            </div>
            <div id="enquiry_forms_wrapper" style="margin-top:8px; width:200px;">Fields marked by * are required</div>
         </div>
         </form>
<?php
}//end if
else
{
    date_default_timezone_set('Europe/London');
    require_once($_SERVER['DOCUMENT_ROOT'].'/php/class.phpmailer.php');
    //All the required fields are filled in, so now we put the email message together and send
    //it to the required email address. If the email fails to send, flag up an error. If the mail
    //sends fine, then display a message declaring success.

    //for testing purposes, use the line below
    $to      = 'mattbridger@googlemail.com';
    //in the live environment, comment out the line above (using '//') and remove the // from below
    //$to      = 'sales@metprep.co.uk';
    $subject = 'Met Prep Enquiry Form Submission';

    //message contents
    $message = '
		<html>
		<head>
		  <title>Met Prep Enquiry Form Submission</title>
		</head>
		<body>
		  <p>The following information was submitted using the Met Prep Enquiry Form :-</p>
		  <table>
		    <tr>
		       <td>Name:</td>
		       <td>'.$_POST['name'].'</td>
		   </tr>
		   <tr>
		        <td>Telephone number:</td>
		        <td>'.$_POST['phone'].'</td>
		   </tr>
		   <tr>
		       <td>Email address:</td>
		       <td>'.$_POST['email'].'</td>
		   </tr>
		   <tr>
		       <td>Enquiry:</td>
		       <td>'.nl2br($_POST['enquiry']).'</td>
		   </tr>
		  </table>
		</body>
</html>';

    $mail                  = new PHPMailer();
    $mail->IsSMTP();                                 // telling the class to use SMTP
    $mail->SMTPAuth        = true;                     // enable SMTP authentication
    $mail->SMTPKeepAlive   = true;                     // SMTP connection will not close after each email sent
    $mail->Host            = "mail.authsmtp.com";   // sets the SMTP server
    $mail->Port            = 23;                       // set the SMTP port for the GMAIL server
    $mail->Username        = "ac29298"; // SMTP account username
    $mail->Password        = "plum5pudd1ng";               // SMTP account password
    $mail->SetFrom('mailmaster@intheuk.com',    'Mailmaster --- Please do not reply to this address');
    $mail->AddReplyTo('mailmaster@intheuk.com', 'Mailmaster --- Please do not reply to this address');
    $mail->Subject         = ($subject);
    $mail->MsgHTML($message);
    $mail->AddAddress($to);


    if ( $mail->Send() )
    {
        //the email has been sent successfully. Display "success" message
?>
<div id="enquiry_form">
            <p><em>Your information has been received and one of our team will be in touch shortly.</em></p><p></p>
          <div id="btn_wrapper_r_close">
             <div id="btn_end_l_r"></div>
             <div id="btn_nav_middle_r">
                <div class="btn_text_r"><a href="#" onclick="window.parent.Shadowbox.close();">Close Window</a></div>
             </div>
             <div id="btn_nav_end_r_r"></div>
          </div>
        </div>
<?php
    }//end if
    else
    {
        //the email failed to send. Display an error mesage
?>
        <div id="enquiry_form">
            <p>An error occured whilst trying to send the information. Email failed to send.</p>
            <p>Please <a href="javascript:history.go(-1);">go back</a>, check your details and try again.</p>
            <p>If you continue to experience problems, please contact us on <a href="mailto:sales@metprep.co.uk">sales@metprep.co.uk</a>, citing your problem and a representative will contact you as soon as possible.</p>
        </div>
<?php
    }//end else
}//end else
?>
      </div>
   </div>
</body>
</html>
