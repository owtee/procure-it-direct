-- phpMyAdmin SQL Dump
-- version 3.3.9.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 24, 2011 at 10:56 AM
-- Server version: 5.1.41
-- PHP Version: 5.2.11

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `p_i_d_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `article_pages`
--

CREATE TABLE IF NOT EXISTS `article_pages` (
  `article_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `article_page_id` int(10) unsigned DEFAULT NULL,
  `article_title` varchar(100) DEFAULT '',
  `article_text` varchar(8000) DEFAULT '',
  `article_side_image` varchar(8000) DEFAULT '',
  PRIMARY KEY (`article_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=68 ;

--
-- Dumping data for table `article_pages`
--

INSERT INTO `article_pages` (`article_id`, `article_page_id`, `article_title`, `article_text`, `article_side_image`) VALUES
(20, 20, 'About PID', '<p><strong>Procure it Direct are procurement and quality control specialists with strategically located offices in Europe, the Middle East and Asia.</strong></p>\r\n<p>Our aim is to supply benchmark quality products from Asia and elsewhere internationally by entering into partnership with first class&nbsp; <br />international manufacturers. This allows uninterrupted access for our clients to otherwise unfamiliar territories and the cost benefits&nbsp; <br />that arise from such partnerships established by PID.</p>\r\n<p>Our current prices are unparalleled in Western markets and our clients have the full satisfaction of contracting with PID an international&nbsp; <br />company and have the added security that we take full responsibility for quality, deliveries and product warranties by partnering the very&nbsp; <br />best international manufacturers.&nbsp;<br /><br /><br /><br />&nbsp;</p>', ''),
(33, 41, 'REMOTE LOCATIONS', '<p>PID are often tasked and can undertake remote location fit out contracts. All too often, clients face lengthy delays, huge cost increases and problems in quality and on time completion.</p>\r\n<p>We have undertaken projects in Moscow, Hvar Island and St Vincent in the Caribbean and have sent dedicated teams into bring projects to successful completion.</p>\r\n<p>We also work for a number of retailers in roll out programmes throughout dedicated geographical locations. This, combined with our procurement division and centralised buying, results in guaranteed completion for our clients with healthy results to their bottom line margins.</p>\r\n<p>&nbsp;&nbsp;</p>\r\n<p>&nbsp;&nbsp;</p>', ''),
(34, 43, 'Installation', '<p>The best interiors leave nothing to chance. We''ll give your new environment the best fit and finish possible - it''s all part of our service.</p>\r\n<p>A superb installation can make all the difference to an interior - and when the work is undertaken by an expert, it can save you time, money, and can guarantee a longer life for your products.</p>\r\n<p>That''s why our service doesn''t stop at sourcing. We will take full contractual responsibility for installing and finishing your new interior, all of which will be documented and specified in a works contract to protect you and your business. This will also ensure that your new environment is fully usable and profitable in a matter of days.</p>\r\n<p>&nbsp;</p>', ''),
(35, 42, 'OUR CLIENTS', '<ul>\r\n<li>Marks &amp; Spencer</li>\r\n<li>Next</li>\r\n<li>Harlequin Hotels and Resorts</li>\r\n<li>Gloria Jeans coffee</li>\r\n<li>Penta Group</li>\r\n<li>Levis</li>\r\n<li>Hilton Hotels</li>\r\n<li>Marriott Hotels</li>\r\n</ul>\r\n<p>&nbsp;&nbsp;&nbsp;</p>\r\n<p>&nbsp;&nbsp;&nbsp;</p>\r\n<p>&nbsp;&nbsp;&nbsp;</p>', ''),
(21, 21, 'OUR BUSINESS MODEL', '<p>We differ from traditional procurement agents in the fact that we have partnership agreements with world class manufacturing companies and we:</p>\r\n<ul>\r\n<li>Accept contractual responsibility for quality</li>\r\n<li>Accept contractual responsibility for deliveries</li>\r\n<li>Accept contractual responsibility for product warranties<br />Contracted directly to the client</li>\r\n<li>Full "in house" QA/QC procedures locally with our partner companies in country of origin</li>\r\n<li>Local international offices to facilitate all cross continent purchasing with the added security of product warranties</li>\r\n</ul>', ''),
(66, 72, 'E Shop', '<p>Pro otevreni naseho eshopu v novem okne kliknete zde - <a title="E SHOP" href="http://eshop.procureitdirect.com" target="_blank">Kliknete zde<br />&nbsp;</a></p>\r\n<p>Procure it Direct are currently only operating their E-Shop in Prague.&nbsp;</p>', ''),
(36, 24, 'PRODUCT LINES', '<p><strong>From brands to bespoke... and beyond.</strong></p>\r\n<p>Looking for an unbeatable price on a branded item or an equal &amp; approved product? Want to stand out from the crowd with a bespoke solution?</p>\r\n<p>Whatever you want to achieve, we''ll ensure you get there. No order is too small or too large for us, and no task is too difficult. Talk to us today to find out how our world-class procurement team can save you time and money and ensure that your business stands out from the crowd. Naturally, we will try to provide samples of all the products we provide prior to final delivery, subject to programme and manufacturing limitations.</p>\r\n<p><strong>How do we do it?</strong><br />In a word: networks.</p>\r\n<p>We know the design and interiors industry inside-out, and our network of suppliers extends throughout Europe and Asia. These people know that we only work with them because their combination of quality and price is unbeatable - a combination which they have to keep to if they want to keep working with us.<br /><br />&nbsp;</p>', ''),
(22, 29, 'Who are Procure it Direct', '<p><strong>xxxxxxProcure it Direct are procurement and quality control specialists with strategically located offices in Europe, the Middle East and Asia.</strong></p>\r\n<p>Our aim is to supply benchmark quality products from Asia and elsewhere internationally by entering into partnership with first class&nbsp;international manufacturers. This allows uninterrupted access for our clients to otherwise unfamiliar territories and the cost benefits that arise from such partnerships established by PID.</p>\r\n<p>Our current prices are unparalleled in Western markets and our clients have the full satisfaction of contracting with PID an international company and have the added security that we take full responsibility for quality, deliveries and product warranties by partnering the very&nbsp; best international manufacturers.</p>', ''),
(67, 74, 'CARIBBEAN', '<p>Hythe House<br />Welches<br />Christ Church BB17154<br />Barbados<br /><br />Tel +1784 533 1925<br /><br />For enquiries or further information please contact Mirko Zapletal at&nbsp;<a style="text-decoration: underline; color: blue; border: 0px initial initial;" href="mailto:miz@procureitdirect.com">miz@procureitdirect.com</a>&nbsp;</p>', ''),
(31, 39, 'CLIENT SATISFACTION', '<p>Our guiding principle is to achieve a high level of client satisfaction by delivering a top quality project completed on time and within budget.</p>\r\n<p>A wealth of management experience in the construction industry has given us the ability to manage refurbishment projects of wide-ranging size and complexity. By understanding the objectives and needs of our clients, we are able to provide a high quality and personal service. We are still a family business and every client is important to us.</p>\r\n<p>Each project is managed by a team of professionals - contract director, estimator, project manager and designsetter-out - backed by skilled craftsmen and closely co-ordinated sub-contractors.</p>\r\n<p>&nbsp;</p>', ''),
(32, 40, 'OUR BUSINESS MODEL', '<p>By utilizing our team of highly experienced people we offer:</p>\r\n<ul>\r\n<li>Full turnkey design and build services</li>\r\n<li>Combine fit out with complete international procurement services obtaining unparalleled prices on certain products for our clients</li>\r\n<li>Value engineering &ndash; Alternative product solutions</li>\r\n<li>Space planning solutions</li>\r\n<li>Central buying and storage solutions for retailers on generalized furniture etc</li>\r\n</ul>\r\n<p>&nbsp;&nbsp;&nbsp;</p>\r\n<p>&nbsp;&nbsp;</p>\r\n<p>&nbsp;</p>', ''),
(23, 22, 'BEST INTERNATIONAL MANUFACTURERS', '<p>We have, since our founding in 2006, worked hard to locate and enter into business relationships with the best international manufacturers<br />in their field.</p>\r\n<p>We have dedicated staff in their factories to ensure that the highest international standards of quality are maintained during all parts of<br />the manufacturing process.</p>\r\n<p>We focus on cost and methods to simplify designs so that we achieve the most economic design solutions and therefore cost saving for our<br />clients while maintaining the design intent.</p>\r\n<p>We establish strict manufacturing timetables and work with our partners on a daily basis to ensure the shipping schedules we entered into with our clients are met.</p>', ''),
(25, 30, 'THE BEST VALUE FOR MONEY', '<p>Our core business is the manufacture and supply of best quality products that include the following:</p>\r\n<ul>\r\n<li>Sanitary ware and accessories</li>\r\n<li>Light fittings and accessories</li>\r\n<li>Finishing materials including flooring, doors, furniture etc</li>\r\n<li>FF&amp;E for schools, hotels and villas (full FF&amp;E packages undertaken)</li>\r\n<li>Kitchens</li>\r\n<li>Raised access flooring</li>\r\n<li>Office furniture</li>\r\n</ul>\r\n<p><strong>All of our partner companies comply with the following criteria:</strong></p>\r\n<ul>\r\n<li>All International ISO 9001 certified and majority ISO 14001</li>\r\n<li>All pre approved and vetted for corporate and social responsibility including use of child labour </li>\r\n<li>We are responsible for all communications and work to well established international standards of service and communications.</li>\r\n</ul>\r\n<p>&nbsp;</p>', ''),
(26, 31, 'SUSTAINABILITY OF PRODUCTS', '<p>Procure it Direct are currently one of the founding members of the Czech Green Building Council <a href="http://www.czgbc.org/">www.czgbc.org</a></p>\r\n<ul>\r\n<li>We are very proactive in our sustainable policies and ensure all our manufacturers conform and adhere to our strict<br />standards</li>\r\n<li>Many of our products are timber based and we only source from companies in China who are FSC registered</li>\r\n<li>One of our largest blue chip clients, Marks &amp; Spencer, are very proactive in their sustainable policies. We have passed and continue to adhere to their strict requirements</li>\r\n</ul>', ''),
(27, 32, 'SUMMARY OF BENEFITS', '<p>We believe that we can make a substantial saving to your overall budget which will make a significant contribution to your bottom line profitability.</p>\r\n<p>We offer turnkey sourcing, ordering, QC, shipping, logistics, import, clearance and delivery to site whilst delivering substantial savings. Flexibility in our approach; ability to store and deliver enhance current supply chain management system allowing you to stay &ldquo;one step ahead of the competition&rdquo;.</p>\r\n<p>A vast majority of the worlds goods are now produced in China (The Factory Of The World) from glass to aero planes. Companies cannot survive without a local office, local HR, stringent local QC procedures and local knowledge of government rules and regulations.</p>', ''),
(24, 23, 'WHEN TO USE PID', '<p>Clearly, the more time available for alternative design solutions for FF&amp;E etc will allow the client to fine tune the design and realise large savings in his construction budget whilst purchasing goods of the highest quality.<br /><br />This will allow sufficient time for preparation of mockup rooms etc for major developments and permit the client to see the actual materials<br />and designs before sign off by the client.<br /><br />We actively encourage our clients to visit our manufacturing facilities to see for themselves the highest standards of manufacturing.</p>', ''),
(28, 27, 'PID fit out service', '<p>PID is one of the most experienced and well respected interior fit out contractors in Central Europe. We specialise in providing high quality<br />refurbishment, bespoke joinery and physical security installations for the residential, retail, commercial and public sectors.</p>\r\n<p>Our guiding principle is to achieve a high level of client satisfaction by delivering a top quality project completed on time and within budget.<br /><br /><br /><br /><br /><br /><br /><br />&nbsp;&nbsp;&nbsp;</p>', ''),
(30, 38, 'FIT OUT SERVICE', '<p>PID is one of the most experienced and well respected interior fit out contractors in Central Europe. We specialise in providing high quality<br />refurbishment, bespoke joinery and physical security installations for the residential, retail, commercial and public sectors.</p>\r\n<p>Our guiding principle is to achieve a high level of client satisfaction by delivering a top quality project completed on time and within budget.</p>', ''),
(29, 35, 'GLOBAL PROCUREMENT', '<p><strong>Global sourcing that''s the best in the world</strong></p>\r\n<p>The key to successful sourcing? Knowing the right people.</p>\r\n<p>With a supplier network as wide as ours, anything is possible.</p>\r\n<p>From bespoke designs, equal and approved and core building materials, we can find anything you need to make your environment stylish, striking and profitable.</p>\r\n<p>The secret to our success? Trust. Our suppliers know we trust them to provide us with the best products at the lowest prices - so it''s in their interests to be as competitive as possible.</p>\r\n<p>And with a database of international, pre-qualified manufacturers, you can trust us to guarantee you the best possible price - saving you up to 40% on your order.</p>\r\n<p>Whatever you are seeking, you can be certain of one thing: we will find it for you - quickly, inexpensively, and personally.<br /><br /><br /><br /><br /><br /><br />&nbsp;</p>', ''),
(37, 25, 'EQUAL, SIMILAR & APPROVED PRODUCTS', '<p>Designer-quality interiors don''t have to come at top-whack prices. Why pay through the nose for a famous marque - when a very similar, equal &amp; approved item can be yours for less than half the price?</p>\r\n<p>Show us what you you''re looking for - and our expert team will source a product which will be of comparable quality and design.</p>', ''),
(38, 44, 'CORE BUILDING PRODUCTS', '<p><strong>Save money on a wide range of core building materials by ordering directly through us. <br /></strong><br />From plywood, lumber and MDF through to steel reinforcement, low VOC paints, stone and marble, we can get you the best possible price from our global network of suppliers - saving you time and money.</p>', ''),
(39, 45, 'MISC PRODUCTS', '<p><span class="Apple" style="widows: 2; text-transform: none; text-indent: 0px; border-collapse: separate; font: medium ''Times New Roman''; white-space: normal; orphans: 2; letter-spacing: normal; color: #000000; word-spacing: 0px; -webkit-border-horizontal-spacing: 0px; -webkit-border-vertical-spacing: 0px; -webkit-text-decorations-in-effect: none; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px;"><span class="Apple" style="text-align: justify; line-height: 15px; font-family: Arial, Helvetica, sans-serif; color: #4f4f4f; font-size: 12px;"><strong>Thanks to our global network of manufacturers and suppliers, you''re only limited by your own imagination.</strong> <br /><br />Providing you provide us with a detailed product specification, we can find the best manufacturer or supplier to supply you with the product or material you need - however unusual. From electromagnets for the world''s first fuel-free engine to solar-powered keyring phone chargers, no request is too unusual.<span class="Apple">&nbsp;</span>Get in touch to&nbsp;find out more...</span></span></p>', ''),
(40, 46, 'Article Heading', 'This is the text for the article', ''),
(41, 47, 'BESPOKE PRODUCTS', '<p><span class="Apple" style="widows: 2; text-transform: none; text-indent: 0px; border-collapse: separate; font: medium ''Times New Roman''; white-space: normal; orphans: 2; letter-spacing: normal; color: #000000; word-spacing: 0px; -webkit-border-horizontal-spacing: 0px; -webkit-border-vertical-spacing: 0px; -webkit-text-decorations-in-effect: none; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px;"><span class="Apple" style="text-align: justify; line-height: 15px; font-family: Arial, Helvetica, sans-serif; color: #4f4f4f; font-size: 12px;">Looking for something really specific? Standing out from the crowd costs less than you think. Either provide us with a detailed product specification, or alternatively, ask our design team to help give you the<span class="Apple">&nbsp;</span><strong style="margin: 0px; padding: 0px;">winning edge</strong>.</span></span></p>', ''),
(42, 48, 'SAMPLE ROOMS & MOCKUPS', '<p>There''s nothing quite like seeing a room set or a product physically there, in your environment, to give you a true idea of its suitability. We can design, build and install complete room sets either in our factory or on your premises to help you achieve the right look - whatever''s best for you. <br /><br />What''s more, we can accompany you to the factory to make any final tweaks before you sign off the design - guaranteeing completion and installation in a matter of weeks, rather than months.</p>', ''),
(43, 49, 'BRANDED PRODUCTS', '<p><span class="Apple" style="widows: 2; text-transform: none; text-indent: 0px; border-collapse: separate; font: medium ''Times New Roman''; white-space: normal; orphans: 2; letter-spacing: normal; color: #000000; word-spacing: 0px; -webkit-border-horizontal-spacing: 0px; -webkit-border-vertical-spacing: 0px; -webkit-text-decorations-in-effect: none; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px;"><span class="Apple" style="text-align: justify; line-height: 15px; font-family: Arial, Helvetica, sans-serif; color: #4f4f4f; font-size: 12px;"><strong>Save money<span class="Apple">&nbsp;</span>on a wide range of branded products by ordering directly through us. <br /></strong><br />We have exclusive relationships with many of the most famous brand names in Interiors - and we can source and supply directly to you for less money and in less time than you ever thought possible.</span></span></p>', ''),
(44, 34, 'SECTORS', '<p><strong>A service that''s as unique as you are.</strong></p>\r\n<p>For businesses that rely on superb surroundings, we''re the logical choice...</p>\r\n<p>Then again, our unbeatable combination of price, quality and speed means we''re an ideal sourcing partner for any business. Whatever your industry or sector, our team of global sourcing experts can find a superb solution that is guaranteed to save you time and money, and keep you at the cutting edge.</p>', ''),
(45, 50, 'FITTINGS, FIXTURES & EQUIPMENT', '<p>One of our core specialities involves designing, manufacturing or sourcing furniture, fixtures and fittings for <strong>hotels and the leisure industry</strong>. <br /><br />Our global supply base means we have access to an unbeatable range of products and materials: we have never failed to deliver exactly according to requirements, in terms of product, price and time.<br /><br />Talk to us about sourcing branded products, equal and approved products, bespoke design solutions and our installation service - or coming soon, browse our online catalogue for a flavour of the huge range of products we can source.</p>', ''),
(46, 51, 'OPERATIONS, SUPPLIES & EQUIPMENT', '<p><span class="Apple" style="widows: 2; text-transform: none; text-indent: 0px; border-collapse: separate; font: medium ''Times New Roman''; white-space: normal; orphans: 2; letter-spacing: normal; color: #000000; word-spacing: 0px; -webkit-border-horizontal-spacing: 0px; -webkit-border-vertical-spacing: 0px; -webkit-text-decorations-in-effect: none; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px;"><span class="Apple" style="text-align: justify; line-height: 15px; font-family: Arial, Helvetica, sans-serif; color: #4f4f4f; font-size: 12px;">In addition to sourcing and installing furniture, fixtures and fittings, we also provide a full OS&amp;E service to the <strong>hotel and leisure industry. <br /></strong><br />From glassware to paper to soap, we can source the very best products at unbeatably low prices, passing on bulk-buying discounts so you reap the full benefit of our supply chain expertise.</span></span></p>', ''),
(47, 52, 'RETAIL', '<p><span class="Apple" style="widows: 2; text-transform: none; text-indent: 0px; border-collapse: separate; font: medium ''Times New Roman''; white-space: normal; orphans: 2; letter-spacing: normal; color: #000000; word-spacing: 0px; -webkit-border-horizontal-spacing: 0px; -webkit-border-vertical-spacing: 0px; -webkit-text-decorations-in-effect: none; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px;"><span class="Apple" style="text-align: left; font-family: Arial, Helvetica, sans-serif; color: #4f4f4f; font-size: 12px;">\r\n<p style="text-align: justify; padding-bottom: 5px; line-height: 15px; margin: 0px; padding-left: 0px; padding-right: 0px; padding-top: 5px;">The world of retail is fast and furious, and cost and time savings are of primary importance.&nbsp;We can completely remove the worry of interior design and strategic sourcing, leaving you to focus on your core business. From shop fit, unitary procurement and sustainable procurement to metalwork, materials, floor, wall and ceiling finishes and lighting, you can rely on us to recommend new and innovative solutions for your retail environment.</p>\r\n<p style="text-align: justify; padding-bottom: 5px; line-height: 15px; margin: 0px; padding-left: 0px; padding-right: 0px; padding-top: 5px;">What''s more, we can advise you on how to make<span class="Apple">&nbsp;</span><em style="margin: 0px; padding: 0px;">major improvements</em><span class="Apple">&nbsp;</span>to your supply chain and procurement process that will make a<span class="Apple">&nbsp;</span><strong style="margin: 0px; padding: 0px;">big</strong><span class="Apple">&nbsp;</span>difference to your bottom line.</p>\r\n</span></span></p>', ''),
(48, 53, 'OFFICE FURNITURE', '<p><strong>We can supply everything you need. Contract quality furniture for less...</strong></p>\r\n<p>Office refits require detailed planning. Every day of disruption costs money - which is why we assign a dedicated consultant and project manager to every single client. <br /><br />Whether you are re-planning an existing office, moving into new premises or simply refurbishing your existing workspace, we will be with you every step of the way to ensure your project runs quickly and smoothly.</p>', ''),
(49, 54, 'DEVELOPERS', '<p>PID provides a one-stop solution to developers all over the world. From design and sourcing to delivery and installation, our team of procurement experts will save you time and money - and create a stunning environment that enhances your business. From conference centres, serviced and individual apartments and university dormitories to timeshare and holiday resorts, health clubs, spas and restaurants, we work to your budget and your specifications. The only thing we are never flexible on is quality.</p>\r\n<p><strong>Holiday resort developers</strong><br />Fixed or loose, OSE or FFE, we understand that products and interiors for holiday resorts need to combine quality with durability. We will provide you with a dedicated consultant who will advise you on a design or a product that will create exactly the effect you are seeking - at exactly the price you need.</p>\r\n<p><strong>Packaged furniture solutions</strong><br />Looking for a packaged furniture solution? Our interior design services can provide you with a range of options that meet your needs precisely - either procuring the exact products you need, or working with you to design a bespoke solution that you can package and re-sell to your own customers.</p>\r\n<p>Whatever your role as a developer, why not talk to us to arrange a free consultation, or to hear about our 3D design visualizations</p>', ''),
(50, 55, 'EDUCATION', '<p><span class="Apple" style="widows: 2; text-transform: none; text-indent: 0px; border-collapse: separate; font: medium ''Times New Roman''; white-space: normal; orphans: 2; letter-spacing: normal; color: #000000; word-spacing: 0px; -webkit-border-horizontal-spacing: 0px; -webkit-border-vertical-spacing: 0px; -webkit-text-decorations-in-effect: none; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px;"><span class="Apple" style="text-align: left; font-family: Arial, Helvetica, sans-serif; color: #4f4f4f; font-size: 12px;">\r\n<p style="text-align: justify; padding-bottom: 5px; line-height: 15px; margin: 0px; padding-left: 0px; padding-right: 0px; padding-top: 5px;">When it comes to sourcing fixtures and fittings for schools, universities and student accommodation, the main criteria are durability and cost. We work with a large number of contractors and local authorities, sourcing and supplying everything from doors and windows to furniture and ironmongery.</p>\r\n<p style="text-align: justify; padding-bottom: 5px; line-height: 15px; margin: 0px; padding-left: 0px; padding-right: 0px; padding-top: 5px;">Demanding environments<span class="Apple">&nbsp;</span>require<span class="Apple">&nbsp;</span>robust solutions<span class="Apple">&nbsp;</span>- and you can rely upon us to supply products that will keep maintenance and replacement to an<span class="Apple">&nbsp;</span><em style="margin: 0px; padding: 0px;">absolute minimum.</em></p>\r\n</span></span></p>', ''),
(51, 56, 'LOCAL AUTHORITIES & CONTRACTORS', '<p><strong>Procure it Direct&nbsp;really is a one-stop solution for the design, sourcing and installation of quality interiors.</strong> <br /><br />Not only will we save you time and money on your order, we also keep completely on top of market trends and sustainable sourcing. This makes us extremely popular with contractors and local authority developers. <br /><br />From sanitary ware, ceramics and shuttering to lighting, cleaning equipment or bespoke items, we can design, source and install the very best products at the very best price.</p>', ''),
(52, 57, 'SUSTAINABLE PROCUREMENT', '<p>Often there is a sustainable, cost effective alternative product out there, helping you reduce your carbon footprint.</p>\r\n<p>Our design and procurement teams can advise and propose alternative products to help offset your carbon footprint.</p>\r\n<p>Our design team work with some of the biggest businesses in the UK advising on total sustainable solutions, from specifying and procuring alternative green products to waste management solutions and whole life job costing.</p>', ''),
(53, 61, 'ENVIRONMENTAL', '<p><strong>Global sourcing - with a heart</strong></p>\r\n<p><strong>A global supply chain needs a worldwide environmental policy...</strong></p>\r\n<p>One of the benefits of operating a global network is that it gives us a birds-eye-view of how our activities are impacting the environment. We are absolutely committed to ensuring that every link in our supply chain meets environmental best practice, and we work closely with all of our suppliers and employees to help them reduce their own carbon footprint. To help them - and us - achieve this, we have developed a robust environmental policy, which is monitored and updated to reflect new best practices and environmental developments, as and when they become available.</p>', ''),
(54, 58, 'POLICIES', '<ul>\r\n<li>We are committed to ensuring our supply chains are fully-sustainable, wherever possible</li>\r\n<li>All prospective suppliers must meet our rigorous environmental qualification criteria before they can become a part of our network</li>\r\n<li>We constantly monitor all of our suppliers and manufacturers and ensure they are adhering to environmental standards and best-practices</li>\r\n<li>We ensure that all of our suppliers have action plans for carbon reduction, and we measure their progress regularly</li>\r\n<li>We actively promote recycling and reduction in emissions and waste across our supply chain, and we ensure all of our suppliers have access to the facilities and tools for achieving this</li>\r\n</ul>\r\n<p>&nbsp;</p>', ''),
(55, 59, 'PEOPLE', '<ul>\r\n<li>All of our suppliers are closely monitored to ensure equal and fair treatment of their staff at all times</li>\r\n<li>We encourage all of our suppliers and manufacturers to collaborate and share their own tips and for reducing their impact on the environmental</li>\r\n<li>We run training programmes for staff to raise their awareness of environmental issues, and to enlist their support in improving our performance and that of our supply chains</li>\r\n<li>We encourage our suppliers to adopt a diverse and inclusive employment strategy, creating openings for people from a wide range of backgrounds</li>\r\n<li>All of our suppliers must have an active Health &amp; Safety policy, with H&amp;S representatives who are knowledgeable and fully-trained</li>\r\n<li>We have a dedicated Management Representative whose role it is to ensure our environmental policies reflect current best-practice, and that they are rigidly adhered to throughout all levels of the company and its supply chain</li>\r\n</ul>', ''),
(56, 60, 'PRODUCTS', '<p>We source and promote a product range which minimises impact on the environment - in terms of production, distribution and usage.<br /><br />We see our supply chain as an opportunity for continuous innovation: identifying new opportunities in cost reduction and efficient manufacturing and distribution, and in the sourcing of raw materials and developing better products. <br /><br />The principles on which our supply chain is based also enable us to refine our supplier criteria - making our whole operation even more environmentally-friendly, right the way through.</p>\r\n<p>&nbsp;</p>', ''),
(57, 70, 'PROCURE IT DIRECT CONTACT DETAILS', '<p>Procure it Direct have offices all over the world so are suitably placed to serve your procurement requirements no matter where your company is based.</p>\r\n<p><strong>Prague Office<br /></strong>Karlinske Namesti 6, 186 00<br />Prague 8<br />Czech Republic<strong><br /></strong><strong>Landline:&nbsp; +420 252 547 924</strong><br /><br /><strong>China Office</strong><br />No. 1068, Office 1707<br />Xingang Dong Rd.<br />Haizhu District-Guangzhou<br />China<br /><strong>landline: +86 20 89231395&nbsp;</strong></p>', ''),
(58, 62, 'CZECH REPUBLIC', '<p>Prague Office, Karlinske Namesti 6, 186 00,<br />Prague 8, Czech Republic<br />Tel: +420 252 547 924<br /><br />For enquiries or further information please contact Mirko Zapletal at&nbsp;<a style="color: blue; text-decoration: underline;" href="mailto:miz@procureitdirect.com">miz@procureitdirect.com</a></p>', ''),
(59, 63, 'DUBAI', '<p>Procure It Direct Ltd<br />Po Box 112950<br />Dubai<br />United Arab Emirates<br /><br />Tel: +971 (0) 4 329 8875<br />Fax: +971 (0) 4 329 8895<br /><br />For enquiries or further information please contact Mirko Zapletal at&nbsp;<a style="text-decoration: underline; color: blue; border: 0px initial initial;" href="mailto:miz@procureitdirect.com">miz@procureitdirect.com</a>&nbsp;</p>', ''),
(60, 64, 'HONG KONG', '<p>Procure It Direct Ltd<br />7/F Shonk Kong Trade Ctr<br />161-167 Des Voeux Rd<br />Central<br />Hong Kong<br /><br />For enquiries or further information please contact Mirko Zapletal at&nbsp;<a style="text-decoration: underline; color: blue; border: 0px initial initial;" href="mailto:miz@procureitdirect.com">miz@procureitdirect.com</a>&nbsp;</p>', ''),
(61, 65, 'CHINA', '<p>Procure It Direct (Hk) Ltd<br />No. 1068, Office 1707<br />Xingang Dong Rd.<br />Haizhu District - Guangzhou<br />China</p>\r\n<p>Tel: +86 20 89231395<br /><br />For enquiries or further information please contact Mirko Zapletal at&nbsp;<a style="text-decoration: underline; color: blue; border: 0px initial initial;" href="mailto:miz@procureitdirect.com">miz@procureitdirect.com</a>&nbsp;</p>', ''),
(62, 66, 'KABUL AFGHANISTAN', '<p>Procure It Direct<br />Share-Naw-Kochai Qasabi<br />1st Lane Left Hand<br />Next To Mtn Office<br />Kabul Afghanistan</p>\r\n<p>Mob: +93 (0)787261343<br />Mob: +93 (0)798788093<br />Mob: +971(0)501514289<br /><br />For enquiries or further information please contact Mirko Zapletal at&nbsp;<a style="text-decoration: underline; color: blue; border: 0px initial initial;" href="mailto:miz@procureitdirect.com">miz@procureitdirect.com</a>&nbsp;</p>', ''),
(63, 67, 'TURKMENISTAN', '<p>Procure It Direct<br />Avenue Archabil 76<br />Ashgabat<br />Turkmenistan 744000</p>\r\n<p>Tel: +99 (312) 217317<br />Fax: +99 (312) 217318<br /><br />For enquiries or further information please contact Mirko Zapletal at&nbsp;<a style="text-decoration: underline; color: blue; border: 0px initial initial;" href="mailto:miz@procureitdirect.com">miz@procureitdirect.com</a>&nbsp;</p>', ''),
(64, 68, 'LONDON', '<p>Procure It Direct Ltd<br />17 Black Friars Lane<br />St Pauls<br />LONDON<br />EC4V 6ER</p>\r\n<p>Tel: +44 (0) 207 248 1238<br />Fax: +44 (0) 207 329 2446</p>', ''),
(65, 69, 'QATAR', '<p>Procure It Direct<br />950 Ibn Seena<br />24 AL-Muntazah<br />P.O.Box 4740<br />Doha<br />Qatar</p>\r\n<p>Tel: +974 4410003<br />Fax: +974 4411162<br /><br />For enquiries or further information please contact Mirko Zapletal at&nbsp;<a style="text-decoration: underline; color: blue; border: 0px initial initial;" href="mailto:miz@procureitdirect.com">miz@procureitdirect.com</a>&nbsp;</p>', '');

-- --------------------------------------------------------

--
-- Table structure for table `banner_pics`
--

CREATE TABLE IF NOT EXISTS `banner_pics` (
  `banner_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `banner_page_id` int(10) unsigned DEFAULT NULL,
  `banner_img` varchar(30) DEFAULT '',
  `banner_alt_tag` varchar(150) DEFAULT '',
  PRIMARY KEY (`banner_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `banner_pics`
--

INSERT INTO `banner_pics` (`banner_id`, `banner_page_id`, `banner_img`, `banner_alt_tag`) VALUES
(20, 20, '1268906673.jpg', 'This is the top pic'),
(21, 27, 'no_image.jpg', ''),
(22, 27, '1268942353.jpg', '');

-- --------------------------------------------------------

--
-- Table structure for table `latest_news`
--

CREATE TABLE IF NOT EXISTS `latest_news` (
  `news_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `news_posted_date` date DEFAULT NULL,
  `news_archive_date` date DEFAULT NULL,
  `news_title` varchar(100) DEFAULT '',
  `news_text` varchar(8000) DEFAULT '',
  `news_snippet_title` varchar(50) DEFAULT '',
  `news_snippet` varchar(500) DEFAULT '',
  `news_image_main` varchar(50) DEFAULT '',
  `news_image_thumb` varchar(50) DEFAULT '',
  `news_activated` varchar(50) DEFAULT '',
  PRIMARY KEY (`news_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=46 ;

--
-- Dumping data for table `latest_news`
--

INSERT INTO `latest_news` (`news_id`, `news_posted_date`, `news_archive_date`, `news_title`, `news_text`, `news_snippet_title`, `news_snippet`, `news_image_main`, `news_image_thumb`, `news_activated`) VALUES
(20, NULL, NULL, 'OPENING OFFICE IN CARDIFF', '<p>Procure it Direct are hoping to open an office in Cardiff later this year.</p>\r\n<p>This would serve the United Kingdom, with key account managers working from this office but looking after worldwide clients.</p>', 'Cardiff Office', 'PID hope to open new UK office in Cardiff Bay', '1269010088.jpg', '1269009888.jpg', 'y'),
(27, NULL, NULL, 'New Website goes live', '<p>We have launched our new website to coincide with our new brochure that are fresh from the printers and are looking very good.</p>\r\n<p>This is to help us forge our hold on the international market of procurement and fit-outs for the hotel &amp; leisure industry to name one...</p>\r\n<p>&nbsp;</p>', 'New Website ', 'It was time PID revamped their new website', '1269010737.jpg', '1269010537.jpg', 'y'),
(29, NULL, NULL, 'New Qatar Partnership', '<p>Procure it Direect are very proud to announce they are forging ahead with their new partnership in Qatar, with Mohammad Awajan Al Hajri.<br /><br />Pictured in photograph - Andy Smith, Kevin O''Brien, Mohammad Awajan Al Hajri and Denzil Earland<br /><br /><br /><br />&nbsp;&nbsp;&nbsp;&nbsp;</p>', 'Qatar Partnership', 'New Qatar Partnership ', '1269272509.jpg', '1269272309.jpg', 'y'),
(22, NULL, NULL, 'PID Attend TC Consult evening...', '<p>Procure it Direct&nbsp;were proud to be invited to the TC Consult evening to celebrate TC''s 3rd Birthday also to meet the who''s who of the Cardiff construction and architecural world. For further details <a href="http://www.youtube.com/watch?v=JT7ziIAy2G4" target="_blank">click here<br /></a>&nbsp;&nbsp;</p>', 'We Attend TC Consult', 'PID Attend TC Consult evening', '1269012196.jpg', '1269011996.jpg', 'y'),
(31, NULL, NULL, 'Project in Caribbean', '<p>Andrew Smith (CEO) and Constantinos Theodossiou (Group Buying Director) spend 3 days visiting many factories in Bali to Pre Qualify Villa and Spa manufactures for project in Caribbean.</p>', 'Project in Caribbean', 'New Project in Caribbean, team to spend 3 days there', '1272550073.jpg', '1272549873.jpg', 'y'),
(32, NULL, NULL, 'Buccament Bay Launch', '<p>Andrew Smith (CEO) attended the launch of Buccament Bay St. Vincent Caribbean in Wembley football ground.</p>', 'Buccament Bay Launch', 'Launch of Buccament Bay St. Vincent Caribbean in Wembley football ground', '1272550242.jpg', '1272550151.jpg', 'y'),
(38, NULL, NULL, 'Appointment of Chris Lee ', '<p>We are delighted to announce that Chris Lee has joined Procure it Direct as our Managing Director for the group.</p>\r\n<div id="_mcePaste" style="position: absolute; left: -10000px; top: 0px; width: 1px; height: 1px; overflow-x: hidden; overflow-y: hidden;">Chris joins us from Fairmont Raffles Hotels International, where he held the position of Executive Director of Design &amp; Construction for the Middle East, Africa and Near Asia. Prior to this he covered the same geographical region with the world largest hotel operator, InterContinental Hotels Group, where he was Director of Technical Services. Both of these positions saw him providing hands on advice to Investors on how to improve or maximise their assets and ranged from financial reviews, all the way through to operational flows and daily liaison with the Design Teams.&nbsp;</div>\r\n<div id="_mcePaste" style="position: absolute; left: -10000px; top: 0px; width: 1px; height: 1px; overflow-x: hidden; overflow-y: hidden;">A seasoned professional in not only the Hospitality Sector, he began his career in construction and Project Management, spending a number of years with the global consultancy EC Harris &ndash; he is well versed in commercial development, construction processes, project and programme management, as well as financial aspects.&nbsp;</div>\r\n<div id="_mcePaste" style="position: absolute; left: -10000px; top: 0px; width: 1px; height: 1px; overflow-x: hidden; overflow-y: hidden;">Andrew Smith, Owner and CEO of Procure it Direct said &ldquo;We are delighted to have Chris on board as our new MD &ndash; not only will the business will be in a safe pair of hands, but with his far reaching knowledge of construction, client management skills and the hotel industry, I am proud to say he is a real addition for the Group.&rdquo;</div>\r\n<p>We are delighted to announce that Chris Lee has joined Procure it Direct as our Managing Director for the group.<br /><br />Chris joins us from Fairmont Raffles Hotels International, where he held the position of Executive Director of Design &amp; Construction for the Middle East, Africa and Near Asia. Prior to this he covered the same geographical region with the world largest hotel operator, InterContinental Hotels Group, where he was Director of Technical Services. Both of these positions saw him providing hands on advice to Investors on how to improve or maximise their assets and ranged from financial reviews, all the way through to operational flows and daily liaison with the Design Teams.&nbsp;<br /><br />A seasoned professional in not only the Hospitality Sector, he began his career in construction and Project Management, spending a number of years with the global consultancy EC Harris &ndash; he is well versed in commercial development, construction processes, project and programme management, as well as financial aspects.&nbsp;<br />Andrew Smith, Owner and CEO of Procure it Direct said &ldquo;We are delighted to have Chris on board as our new MD &ndash; not only will the business will be in a safe pair of hands, but with his far reaching knowledge of construction, client management skills and the hotel industry, I am proud to say he is a real addition for the Group.&rdquo;</p>', 'Chris Lee joins PiD', 'We are delighted to announce that Chris Lee has joined Procure it Direct', '1319118370.jpg', '1319118170.jpg', 'y'),
(40, NULL, NULL, 'PID Prague move ', '<p>Continued expansion of Procure it Direct has seen PiD relocate their offices in Prague.</p>\r\n<div id="_mcePaste" style="position: absolute; left: -10000px; top: 0px; width: 1px; height: 1px; overflow-x: hidden; overflow-y: hidden;">Moving from the outskirts of Prague to a more centralised location, Managing Director, Chris Lee commented &ldquo;Being at the heart of the city is key to the vast majority of businesses &ndash; Karlinske Namesti is a great location and is the epicentre for commercial and retail FF&amp;E activities&rdquo;.</div>\r\n<p>Continued expansion of Procure it Direct has seen PiD relocate their offices in Prague.<br /><br />Moving from the outskirts of Prague to a more centralised location, Managing Director, Chris Lee commented &ldquo;Being at the heart of the city is key to the vast majority of businesses &ndash; Karlinske Namesti is a great location and is the epicentre for commercial and retail FF&amp;E activities&rdquo;.</p>', 'PID Prague move ', 'Continued expansion of Procure it Direct', '1319118746.jpg', '1319118546.jpg', 'y'),
(41, NULL, NULL, 'H Hotel in Barbados', '<p>PiD have just been awarded a full turnkey procurement contract for H hotel, Barbados. H Barbados occupies a seafront position at Hastings Beach in Christ Church on the southwest coast of Barbados with an outdoor swimming pool and 70 studio suites. It will have a Jack&rsquo;s Steak &amp; Seafood restaurant serving delicious local seafood and the finest cuts of prime, succulent steaks, a swimming pool with an outdoor cocktail lounge, spa pavilion &amp; fitness centre, an HQ coffee shop and a boutique. H is a new, up and coming brand of boutique hotels owned and operated by Harlequin Hotels and Resorts. The property is due to open its doors to the Public on October 3rd 2012 and is poised to become the jewel of Barbados</p>\r\n<div id="_mcePaste" style="position: absolute; left: -10000px; top: 0px; width: 1px; height: 1px; overflow-x: hidden; overflow-y: hidden;">Andrew Smith CEO of Procure it Direct said &ldquo; its always rewarding to be given repeat business by a client and reinforces our ability to manage our clients expectations successfully.&rdquo;</div>\r\n<div id="_mcePaste" style="position: absolute; left: -10000px; top: 0px; width: 1px; height: 1px; overflow-x: hidden; overflow-y: hidden;">http://www.harlequinhotelsandresorts.com/hotels/</div>\r\n<p>PiD have just been awarded a full turnkey procurement contract for H hotel, Barbados. H Barbados occupies a seafront position at Hastings Beach in Christ Church on the southwest coast of Barbados with an outdoor swimming pool and 70 studio suites. <br /><br />It will have a Jack&rsquo;s Steak &amp; Seafood restaurant serving delicious local seafood and the finest cuts of prime, succulent steaks, a swimming pool with an outdoor cocktail lounge, spa pavilion &amp; fitness centre, an HQ coffee shop and a boutique. <br /><br />H is a new, up and coming brand of boutique hotels owned and operated by Harlequin Hotels and Resorts. The property is due to open its doors to the Public on October 3rd 2012 and is poised to become the jewel of Barbados<br /><br />Andrew Smith CEO of Procure it Direct said &ldquo; its always rewarding to be given repeat business by a client and reinforces our ability to manage our clients expectations successfully.&rdquo;<br /><br /><a href="http://www.harlequinhotelsandresorts.com/hotels/">http://www.harlequinhotelsandresorts.com/hotels/</a>&nbsp;</p>', 'H Hotel in Barbados ', 'H Hotel in Barbados â€“ PiD awarded procurement contract', '1319119106.jpg', '1319118906.jpg', 'y'),
(43, NULL, NULL, 'Mirko Zapletal joins Procure it Direct â€“ Prague', '<p>Our Central European office, based in Prague, Czech Republic, is pleased to welcome Mirko Zapletal to the Company.</p>\r\n<div id="_mcePaste" style="position: absolute; left: -10000px; top: 0px; width: 1px; height: 1px; overflow-x: hidden; overflow-y: hidden;">Mirko&rsquo;s key responsibilities as Business Analyst is to support the regional office in new opportunities, financial efficiency and implement best practice proceedures throughout the Procure it Direct Group.</div>\r\n<div id="_mcePaste" style="position: absolute; left: -10000px; top: 0px; width: 1px; height: 1px; overflow-x: hidden; overflow-y: hidden;">He has recently completed his Bachelor degree at the California State University in Fresno, majoring in Business Administration - Mirko was named the Outstanding Graduate of the Financial Department at the graduation ceremony.&nbsp;</div>\r\n<div id="_mcePaste" style="position: absolute; left: -10000px; top: 0px; width: 1px; height: 1px; overflow-x: hidden; overflow-y: hidden;">In parallel with his responsibilities at PiD he is working on his Master degree at the University of Prague, in cooperation with the City University of Seattle.&nbsp;</div>\r\n<div id="_mcePaste" style="position: absolute; left: -10000px; top: 0px; width: 1px; height: 1px; overflow-x: hidden; overflow-y: hidden;">A native Czech and keen tennis fanatic, Mirko said he was &ldquo;really pleased to join the Group and looking forward to playing an important role in this very forward thinking Company&rdquo;.</div>\r\n<p>Our Central European office, based in Prague, Czech Republic, is pleased to welcome Mirko Zapletal to the Company.Mirko&rsquo;s key responsibilities as Business Analyst is to support the regional office in new opportunities, financial efficiency and implement best practice proceedures throughout the Procure it Direct Group.He has recently completed his Bachelor degree at the California State University in Fresno, majoring in Business Administration - Mirko was named the Outstanding Graduate of the Financial Department at the graduation ceremony.&nbsp;<br /><br />In parallel with his responsibilities at PiD he is working on his Master degree at the University of Prague, in cooperation with the City University of Seattle.&nbsp;<br /><br />A native Czech and keen tennis fanatic, Mirko said he was &ldquo;really pleased to join the Group and looking forward to playing an important role in this very forward thinking Company&rdquo;.</p>', 'Mirko Zapletal joins', 'Mirko Zapletal joins Procure it Direct â€“ Prague', '1319119971.jpg', '1319119771.jpg', 'y'),
(42, NULL, NULL, 'PiD Executives visit China', '<p>CEO, Andrew Smith and newly installed Managing Director, Chris Lee, recently visited the Peoples Republic of China to embellish trade relations between PiD&rsquo;s key vendors and suppliers. Accompanied by Group Buying Director, Constantinos Theodossiou, the visit saw three days of meetings, factory tours and receptions with key Chinese Executives.&nbsp;</p>\r\n<div id="_mcePaste" style="position: absolute; left: -10000px; top: 0px; width: 1px; height: 1px; overflow-x: hidden; overflow-y: hidden;">Theodossiou, who is based in Guangzhou, said &ldquo;Developing relationships with our vendors is one thing, but maintaining them is equally as important &ndash; with Andrew and Chris taking time from their schedules to meet company dignitaries only goes to reinforce how serious we take these business associations with our supply chain.&rdquo;</div>\r\n<div id="_mcePaste" style="position: absolute; left: -10000px; top: 0px; width: 1px; height: 1px; overflow-x: hidden; overflow-y: hidden;">Procure it Direct have a database of over 4,500 Approved Vendors throughout PRC, along with hundreds of other suppliers, manufacturers and specialist producers throughout Asia.</div>\r\n<p>CEO, Andrew Smith and newly installed Managing Director, Chris Lee, recently visited the Peoples Republic of China to embellish trade relations between PiD&rsquo;s key vendors and suppliers. Accompanied by Group Buying Director, Constantinos Theodossiou, the visit saw three days of meetings, factory tours and receptions with key Chinese Executives.&nbsp;<br /><br />Theodossiou, who is based in Guangzhou, said &ldquo;Developing relationships with our vendors is one thing, but maintaining them is equally as important &ndash; with Andrew and Chris taking time from their schedules to meet company dignitaries only goes to reinforce how serious we take these business associations with our supply chain.&rdquo;<br />Procure it Direct have a database of over 4,500 Approved Vendors throughout PRC, along with hundreds of other suppliers, manufacturers and specialist producers throughout Asia.</p>', 'Executives in China', 'PiD Executives visit China', '1319119422.jpg', '1319119222.jpg', 'y'),
(44, NULL, NULL, 'Grand Opening of St Vincent', '<p>PiD are very proud to have attended the Grand Opening of phase 1 of the Buccament Bay project on the beautiful island of St Vincent &amp; the Grenadines. We supplied all the FF&amp;E, OS&amp;E, Lighting, Sanitaryware, flooring, building materials, general supply goods, BOH equipment and other miscellaneous items. The contracted value was $10m USD. With the property positioned in the upper upscale category, we managed to bring in the FF&amp;E at an average of $7,500 USD, which will give you a good demonstration of the savings we are able to achieve.</p>\r\n<div id="_mcePaste" style="position: absolute; left: -10000px; top: 0px; width: 1px; height: 1px; overflow-x: hidden; overflow-y: hidden;">Andrew Newman, Operations Director, Procure it Direct said &ldquo; From a timing perspective we completed the manufacturing of all the goods in 12 weeks from receipt of Interior Designers detailed drawings, followed by a carefully planned shipping programme spread over 7 weeks. The freight consisted of 32,500 items and amounted to 168 No. 40'' containers. All goods were supplied from our network of manufacturing partners based in and around Asia / China, constructed to the specifications set out by the interior designer along with the necessary quality, specifications and international warranties.&rdquo;</div>\r\n<div id="_mcePaste" style="position: absolute; left: -10000px; top: 0px; width: 1px; height: 1px; overflow-x: hidden; overflow-y: hidden;">www.buccamentbay.com</div>\r\n<p><br />PiD are very proud to have attended the Grand Opening of phase 1 of the Buccament Bay project on the beautiful island of St Vincent &amp; the Grenadines. We supplied all the FF&amp;E, OS&amp;E, Lighting, Sanitaryware, flooring, building materials, general supply goods, BOH equipment and other miscellaneous items. <br /><br />The contracted value was $10m USD. With the property positioned in the upper upscale category, we managed to bring in the FF&amp;E at an average of $7,500 USD, which will give you a good demonstration of the savings we are able to achieve.<br /><br />Andrew Newman, Operations Director, Procure it Direct said &ldquo; From a timing perspective we completed the manufacturing of all the goods in 12 weeks from receipt of Interior Designers detailed drawings, followed by a carefully planned shipping programme spread over 7 weeks. The freight consisted of 32,500 items and amounted to 168 No. 40'' containers. All goods were supplied from our network of manufacturing partners based in and around Asia / China, constructed to the specifications set out by the interior designer along with the necessary quality, specifications and international warranties.&rdquo;<br /><br /><a href="http://www.buccamentbay.com">www.buccamentbay.com</a></p>', 'Grand Opening', 'Grand Opening of St Vincent', '1319120283.jpg', '1319120083.jpg', 'y'),
(45, NULL, NULL, 'Kara Wutzke joins', '<p>We are delighted to welcome Kara Wutzke to the Procure it Direct family, where she joins us as Operations Manager &ndash; APAC.</p>\r\n<div id="_mcePaste" style="position: absolute; left: -10000px; top: 0px; width: 1px; height: 1px; overflow-x: hidden; overflow-y: hidden;">Kara Wutzke has over 8 years of impressive Quality Control experience in the fields of manufacturing, product development and Quality Control in the Chinese market, covering a whole host of sectors that range from heavy industry manufacturing, clothing and FF&amp;E / Furniture sectors with major foreign companies.&nbsp;</div>\r\n<div id="_mcePaste" style="position: absolute; left: -10000px; top: 0px; width: 1px; height: 1px; overflow-x: hidden; overflow-y: hidden;">&ldquo;What attracted me to PiD is the mere fact they took QC and QA so seriously &ndash; with my background and training in this area of expertise, I was thrilled to be given an opportunity to strenghthen the QA / QC team, but assist on the day to day management of operations&rdquo;.</div>\r\n<div id="_mcePaste" style="position: absolute; left: -10000px; top: 0px; width: 1px; height: 1px; overflow-x: hidden; overflow-y: hidden;">Having lived in PRC for a number of years, Wutzke is fluent in Mandarin Chinese. In addition to this asset, she is proficient in contract negotiation skills, procurement frameworks and monitoring project procurement from start to completion.</div>\r\n<p>We are delighted to welcome Kara Wutzke to the Procure it Direct family, where she joins us as Operations Manager &ndash; APAC.<br /><br />Kara Wutzke has over 8 years of impressive Quality Control experience in the fields of manufacturing, product development and Quality Control in the Chinese market, covering a whole host of sectors that range from heavy industry manufacturing, clothing and FF&amp;E / Furniture sectors with major foreign companies.&nbsp;<br /><br />&ldquo;What attracted me to PiD is the mere fact they took QC and QA so seriously &ndash; with my background and training in this area of expertise, I was thrilled to be given an opportunity to strenghthen the QA / QC team, but assist on the day to day management of operations&rdquo;.<br /><br />Having lived in PRC for a number of years, Wutzke is fluent in Mandarin Chinese. In addition to this asset, she is proficient in contract negotiation skills, procurement frameworks and monitoring project procurement from start to completion.</p>', 'Kara Wutzke joins', 'Kara Wutzke joins Procure it Direct - China', '1319120576.jpg', '1319120376.jpg', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `left_side_snippets`
--

CREATE TABLE IF NOT EXISTS `left_side_snippets` (
  `snippet_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `snippet_title` varchar(50) DEFAULT '',
  `snippet_text` varchar(50) DEFAULT '',
  `snippet_symbol` varchar(50) DEFAULT '',
  `snippet_url` varchar(500) DEFAULT '',
  `snippet_article` varchar(8000) DEFAULT '',
  `snippet_article_title` varchar(80) DEFAULT '',
  `snippet_views` int(10) unsigned DEFAULT NULL,
  `snippet_activated` varchar(50) DEFAULT '',
  PRIMARY KEY (`snippet_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `left_side_snippets`
--

INSERT INTO `left_side_snippets` (`snippet_id`, `snippet_title`, `snippet_text`, `snippet_symbol`, `snippet_url`, `snippet_article`, `snippet_article_title`, `snippet_views`, `snippet_activated`) VALUES
(20, 'We are cheaper', 'find out here', '&pound;', '', '<p><strong>We get asked this a lot.</strong><br /><br />Our prices are regularly such good value our customers ask us "Is there a problem with the quality?"<br /><br />The answer is simple. We invite customers to visit our manufacturers and see for themselves.<br /><br />We invite you to join us to satisfy yourself we can achieve fabulous quality for less.</p>', 'WHY WE ARE CHEAPER', 560205, 'y'),
(21, 'Got a query', 'please get in touch', '?', '', '<p>We know the importance of a prompt response. We will contact you within 24 hours of receiving your enquiry - please send an email to <a href="mailto:info@procureitdirect.com">info@procureitdirect.com</a></p>\r\n<p>One of our project managers will contact you.</p>', 'DO YOU HAVE A PROJECT', 560202, 'y'),
(22, 'Hotelier Savings', 'find out more', '$', '', '<ul>\r\n<li>Reduce and stabilise your energy bills (with renewable energy)</li>\r\n<li>Make use of green energy incentives</li>\r\n<li>Find out about green energy incentives</li>\r\n<li>Let us make use of your natural environment</li>\r\n<li>Recover up to 97% of your heat generated</li>\r\n<li>Wipe thousands off your energy bills</li>\r\n<li>Interest free finance for renewable energy systems</li>\r\n<li>Extend the life of your heating equipment</li>\r\n<li>PR your consumer by reducing your CO2 emissions</li>\r\n<li>Contact us to arrange an appointment</li>\r\n</ul>', 'SAVE MONEY NOW', 560202, 'y');

-- --------------------------------------------------------

--
-- Table structure for table `menu_lines`
--

CREATE TABLE IF NOT EXISTS `menu_lines` (
  `menu_line_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_line_parent` int(10) unsigned DEFAULT NULL,
  `menu_line_sequence` int(10) unsigned DEFAULT NULL,
  `menu_line_title` varchar(70) DEFAULT '',
  `menu_line_page_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`menu_line_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=59 ;

--
-- Dumping data for table `menu_lines`
--

INSERT INTO `menu_lines` (`menu_line_id`, `menu_line_parent`, `menu_line_sequence`, `menu_line_title`, `menu_line_page_id`) VALUES
(1, 0, 1, 'About PID', 20),
(2, 0, 4, 'Global Procurement', 35),
(4, 0, 5, 'Product Lines', 24),
(5, 0, 6, 'Sectors', 34),
(6, 0, 7, 'Environmental', 61),
(7, 0, 11, 'Contact Details', 70),
(8, 0, 8, 'News', 26),
(9, 0, 9, 'Latest projects', 28),
(58, 7, 9, 'Caribbean', 74),
(11, 1, 2, 'Our business model', 21),
(12, 1, 3, 'Best international manufacturers', 22),
(13, 1, 4, 'When to use PID', 23),
(14, 1, 6, 'Sustainability of products', 31),
(15, 1, 5, 'The best value for money', 30),
(16, 1, 7, 'Summary of benefits', 32),
(19, 4, 1, 'Equal Similar and Approved Products', 25),
(20, 4, 2, 'Core Building Products', 44),
(21, 4, 3, 'Misc Products', 45),
(23, 4, 5, 'Bespoke Products', 47),
(24, 4, 6, 'Sample Rooms & Mockups', 48),
(25, 4, 7, 'Branded Products', 49),
(30, 0, 2, 'Fit Out Service', 38),
(31, 30, 1, 'Client Satisfaction', 39),
(32, 30, 2, 'Our Business Model', 40),
(33, 30, 3, 'Remote Locations', 41),
(34, 30, 5, 'Our Clients', 42),
(35, 30, 4, 'Installation', 43),
(36, 5, 1, 'FF&E', 50),
(37, 5, 2, 'Operations, supplies & equipment', 51),
(38, 5, 3, 'Retail', 52),
(39, 5, 4, 'Office Furniture', 53),
(40, 5, 5, 'Developers', 54),
(41, 5, 6, 'Education', 55),
(42, 5, 7, 'Local authorities & contractor supply', 56),
(43, 5, 8, 'Sustainable procurement', 57),
(44, 6, 2, 'Policies', 58),
(45, 6, 4, 'People', 59),
(46, 6, 3, 'Products', 60),
(47, 7, 1, 'Czech Republic', 62),
(48, 7, 2, 'Dubai', 63),
(49, 7, 3, 'Hong Kong', 64),
(50, 7, 4, 'China', 65),
(51, 7, 5, 'Kabul Afghanistan', 66),
(52, 7, 6, 'Turkmenistan', 67),
(54, 7, 8, 'Qatar', 69),
(57, 8, 1, 'Seminar in Cardiff, Wales', 73);

-- --------------------------------------------------------

--
-- Table structure for table `movie_pics`
--

CREATE TABLE IF NOT EXISTS `movie_pics` (
  `movie_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `movie_page_id` int(10) unsigned DEFAULT NULL,
  `movie_img` varchar(30) DEFAULT '',
  `movie_alt_tag` varchar(150) DEFAULT '',
  PRIMARY KEY (`movie_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=60 ;

--
-- Dumping data for table `movie_pics`
--

INSERT INTO `movie_pics` (`movie_id`, `movie_page_id`, `movie_img`, `movie_alt_tag`) VALUES
(20, 20, '1269001954.swf', 'Flash Banner About Us'),
(21, 24, '1269003165.swf', 'Our Team'),
(22, 26, '1269004223.swf', ''),
(23, 999, '1268942821.jpg', ''),
(24, 29, '1268953341.swf', 'Procure it Direct'),
(25, 34, '1269003536.swf', ''),
(26, 38, '1269002716.swf', ''),
(27, 61, '1269004178.swf', ''),
(28, 70, '1269007829.swf', ''),
(29, 21, '1269004483.jpg', ''),
(30, 22, '1269004568.jpg', ''),
(31, 23, '1269004706.jpg', ''),
(32, 30, '1269004831.jpg', ''),
(33, 31, '1269004927.jpg', ''),
(34, 32, '1269005002.jpg', ''),
(35, 25, '1269005184.jpg', ''),
(36, 44, '1269005477.jpg', ''),
(37, 45, '1269005563.jpg', ''),
(38, 47, '1269005657.jpg', ''),
(39, 48, '1269005779.jpg', ''),
(40, 49, '1269005889.jpg', ''),
(41, 50, '1269006017.jpg', ''),
(42, 51, '1269006031.jpg', ''),
(43, 52, '1269006069.jpg', ''),
(44, 53, '1269006171.jpg', ''),
(45, 54, '1269006342.jpg', ''),
(46, 55, '1269006506.jpg', ''),
(47, 56, '1269006656.jpg', ''),
(48, 57, '1269006685.jpg', ''),
(49, 58, '1269006723.jpg', ''),
(50, 59, '1269006933.jpg', ''),
(51, 60, '1269006950.jpg', ''),
(52, 62, '1269007365.jpg', ''),
(53, 63, '1269007447.jpg', ''),
(54, 64, '1269007498.jpg', ''),
(55, 65, '1269007511.jpg', ''),
(56, 66, '1269007523.jpg', ''),
(57, 67, '1269007535.jpg', ''),
(58, 68, '1269007620.jpg', ''),
(59, 69, '1269007765.jpg', '');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE IF NOT EXISTS `projects` (
  `project_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_title` varchar(100) DEFAULT '',
  `project_snippet` varchar(100) DEFAULT '',
  `project_main_title` varchar(200) DEFAULT '',
  `project_text` varchar(8000) DEFAULT '',
  `project_thumb_image` varchar(150) DEFAULT '',
  `project_main_image` varchar(150) DEFAULT '',
  `project_country` varchar(30) DEFAULT '',
  `project_activated` varchar(30) DEFAULT '',
  PRIMARY KEY (`project_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`project_id`, `project_title`, `project_snippet`, `project_main_title`, `project_text`, `project_thumb_image`, `project_main_image`, `project_country`, `project_activated`) VALUES
(20, 'SC Campus Brno', 'Contructional, Sanitary Installation and Pluming, electrics, HVAC', 'SC Campus Brno', '<ul>\r\n<li>Shopping centre: SC Campus Brno&nbsp;&nbsp;&nbsp;&nbsp;</li>\r\n<li>Area: 1000 m2</li>\r\n<li>Term of realization: 2008-2009&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>\r\n<li>Turn-over: 7,6 mil Kc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>\r\n<li>Subject of delivery: Contructional, Sanitary</li>\r\n<li>Installation and Pluming, electrics, HVAC&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>\r\n<li>Coordination of supplier: Paving, lightening&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>\r\n<li>Investor: Marks and Spencer Czech Republic, a.s.&nbsp;&nbsp;&nbsp;<br /><br /><br /><br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>\r\n</ul>', '1269268529.jpg', '1269268779.jpg', 'PRAGUE', 'y'),
(21, 'SC Avion Ostrava', 'Contructional, Electrics, HVAC, Step-layer, Furniture, Protection of goods', 'SC AVION OSTRAVA', '<ul>\r\n<li>Shopping centre: SC Avion Ostrava&nbsp;&nbsp;&nbsp;&nbsp;</li>\r\n<li>Area: 300 m2</li>\r\n<li>Term of realization: 2008&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>\r\n<li>Turn-over: 4,5 mil Kc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>\r\n<li>Subject of delivery: Contructional, Electrics, HVAC,</li>\r\n<li>Step-layer, Furniture, Protection of goods, Lightning,</li>\r\n<li>Sprinkler system, Electrical Fire-Warning&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>\r\n<li>Investor: Central Pacific Investments, a.s.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>\r\n</ul>\r\n<p><br /><br />&nbsp;&nbsp;&nbsp;&nbsp;</p>', '1269268846.jpg', '1269269096.jpg', 'PRAGUE', 'y'),
(22, 'HOT JEWELS', 'Furniture, Lightning, Floor, Sprinkler System, Logos', 'HOT JEWELS', '<ul>\r\n<li>Shopping centre: SC Metropole Zliè&iacute;n&nbsp;&nbsp;&nbsp;&nbsp;</li>\r\n<li>Area: Display stand</li>\r\n<li>Term of realization: 2008&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>\r\n<li>Turn-over: 530,000&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>\r\n<li>Subject of delivery: Furniture, Lightning, Floor,</li>\r\n<li>Sprinkler System, Logos&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>\r\n<li>Investor: Hot Jewels s.r.o.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>', '1269269166.jpg', '1269269416.jpg', 'PRAGUE', 'y'),
(23, 'LEVI''S', 'Constructional, Sanitary Installation and Plumbing, Electrics...', 'LEVI''S', '<ul>\r\n<li>Shopping centre: OC Ark&aacute;dy Pankr&aacute;c</li>\r\n<li>Plocha:&nbsp;130 m2</li>\r\n<li>Term of realization: 2008&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>\r\n<li>Turn-over: 2,3 mil Kc&nbsp;&nbsp;&nbsp;&nbsp;</li>\r\n<li>Subject of delivery: Constructional, Sanitary</li>\r\n<li>Installation and Plumbing, Electrics, Climate, Parquet&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>\r\n<li>Coordination of supplier: Lighting, Furniture&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>\r\n<li>Investor: LEVI STRASS PRAHA, spol. s r.o.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>\r\n</ul>', '1269269557.jpg', '1269269807.jpg', 'PRAGUE', 'y'),
(24, 'PLEASURE STATE', 'Electrics, Furniture, Security of Goods, Logos, Lighting...', 'PLEASURE STATE', '<ul>\r\n<li>Shopping centre: Department Store Kotva&nbsp;&nbsp;&nbsp;&nbsp;</li>\r\n<li>Area: 75 m2</li>\r\n<li>Term of realization: 2009&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>\r\n<li>Turn-over: 1,6 mil CZK&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>\r\n<li>Subject of delivery: Electrics, Furniture, Security of Goods, Logos, Lighting, Type elements&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>\r\n<li>Investor: Secret s r.o.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>\r\n</ul>\r\n<p>&nbsp;</p>', '1269269884.jpg', '1269270134.jpg', 'PRAGUE', 'y'),
(25, 'Marks & Spencer', 'Contructional, Sanitary Installation and Pluming, Electrics', 'Marks & Spencer', '<ul>\r\n<li>Shopping centre: SC Avion Shopping Park Ostrava&nbsp;&nbsp;&nbsp;&nbsp;</li>\r\n<li>Area: 2100 m2</li>\r\n<li>Term of realization: 2008&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>\r\n<li>Turn-over: 11,6 mil Kc&nbsp;&nbsp;&nbsp;&nbsp;</li>\r\n<li>Subject of delivery: Contructional, Sanitary Installation and Pluming, Electrics&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>\r\n<li>Coordination of suplliers: Paving, Lightning, Show-case, HVAC&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>\r\n<li>Investor: Marks and Spencer Czech Republic, a.s.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>\r\n</ul>', '1269270306.jpg', '1269270556.jpg', 'PRAGUE', 'y'),
(26, 'Buccament Bay', 'St Vincent is a volcanic island.', 'Buccament Bay', '<p>Population 110,000.<br />Capital Kingstown.</p>\r\n<p>Area 344 sq km/133 sq miles; 29 km/18 miles long 18 km/11 miles wide.</p>\r\n<p>Geography St Vincent is a volcanic island. The northern third of the island is mountainous (parts of this area are accessible only by boat because roads cannot<br />be built on the terrain). The highest point is La Soufriere volcano - 1,234 metres.</p>\r\n<p>Most of the island&rsquo;s population lives near the southern coast. The island has a total of 84 km of coastline.<br />The island has 5 administrative parishes &ndash; Charlotte, St Andrew, St David, St George, St Patrick</p>\r\n<p>Time difference GMT -4.</p>\r\n<p>Language Official language is English.</p>\r\n<p>Monetary unit Eastern Caribbean dollar - EC$ (fixed to the US dollar); US dollars are widely accepted.</p>\r\n<p>Tourism St Vincent is a new, unspoilt tourist destination. It is currently visited by ocean cruisers from the UK &amp; US for day trips. High season is January to May. There are approx 400 rooms on the island, and no hotel is above a 3 Star.</p>', '1269271135.jpg', '1269271385.jpg', 'CARIBBEAN', 'y'),
(27, '', '', '', '', '', '', '', '1274280929');

-- --------------------------------------------------------

--
-- Table structure for table `site_pages`
--

CREATE TABLE IF NOT EXISTS `site_pages` (
  `page_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `page_url` varchar(200) DEFAULT '',
  `page_title` varchar(200) DEFAULT '',
  `page_type` int(10) unsigned DEFAULT NULL,
  `page_keywords` varchar(1500) DEFAULT '',
  `page_description` varchar(1500) DEFAULT '',
  `page_abstract` varchar(700) DEFAULT '',
  `page_name` varchar(700) DEFAULT '',
  PRIMARY KEY (`page_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=75 ;

--
-- Dumping data for table `site_pages`
--

INSERT INTO `site_pages` (`page_id`, `page_url`, `page_title`, `page_type`, `page_keywords`, `page_description`, `page_abstract`, `page_name`) VALUES
(20, 'About_PID.php?mpid=0&mlid=1', 'About PID', 1, 'About PID', 'About PID', '', 'About PID'),
(21, 'Our_Team.php?mpid=1&mlid=11', 'Our business model', 1, 'Our business model', 'Our business model', '', 'Our business model'),
(22, 'Architectural_and_Interior_Design.php?mpid=1&mlid=12', 'Best international manufacturers', 1, 'Best international manufacturers', 'Best international manufacturers', '', 'Best international manufacturers'),
(23, 'Environmental.php?mpid=1&mlid=13', 'When to use PID', 1, 'When to use Procure it Direct', 'When to use PID', '', 'When to use PID'),
(24, 'Product_Lines.php?mpid=0&mlid=4', 'Product Lines', 1, 'Product Lines', 'Product Lines', '', 'Product Lines'),
(25, 'Equal_Similar_and_Approved_Products.php?mpid=4&mlid=19', 'Equal Similar and Approved Products', 1, 'Equal Similar and Approved Products', 'Equal Similar and Approved Products', '', 'Equal Similar and Approved Products'),
(26, 'News.php?mpid=0&mlid=8', 'News', 4, 'News', 'News', '', 'News'),
(27, 'Installations.php?mpid=0&mlid=3', 'Fit Out Service', 2, 'Fit Out Service', 'Fit Out Service', '', 'Fit Out Service'),
(28, 'Latest_projects.php?mpid=0&mlid=9', 'Latest projects', 3, 'Latest projects', 'Latest projects', '', 'Latest projects'),
(29, '1268952976.php?mpid=1&mlid=26', 'Who are Procure it Direct', 1, 'About Procure it Direct', 'Who are Procure it Direct', '', 'Who are Procure it Direct'),
(30, 'The_best_value_for_money.php?mpid=1&mlid=15', 'The best value for money', 1, 'The best value for money', 'The best value for money', '', 'The best value for money'),
(31, 'Sustainability_of_products.php?mpid=1&mlid=14', 'Sustainability of products', 1, 'Sustainability of products', 'Sustainability of products', '', 'Sustainability of products'),
(32, 'Summary_of_benefits.php?mpid=1&mlid=16', 'Summary of benefits', 1, 'Summary of benefits', 'Summary of benefits', '', 'Summary of benefits'),
(33, 'Global_Procurement.php?mpid=1&mlid=27', 'Global Procurement', 2, 'Global Procurement', 'Global Procurement', '', 'Global Procurement'),
(34, 'Sectors.php?mpid=0&mlid=5', 'Sectors', 1, 'v', 'Sectors', '', 'Sectors'),
(35, '1268956433.php?mpid=0&mlid=2', 'Global Procurement', 2, 'Global Procurement', 'Global Procurement', '', 'Global Procurement'),
(36, 'fred.php?mpid=1&mlid=28', 'fred', 1, 'fred', 'fred', '', 'fred'),
(37, 'Client_Satisfaction.php?mpid=3&mlid=29', 'Client Satisfaction', 1, 'Client Satisfaction', 'Client Satisfaction', '', 'Client Satisfaction'),
(38, 'Fit_Out_Service.php?mpid=0&mlid=30', 'Fit Out Service', 1, 'Fit Out Service', 'Fit Out Service', '', 'Fit Out Service'),
(39, '1268991532.php?mpid=30&mlid=31', 'Client Satisfaction', 1, 'Client Satisfaction', 'Client Satisfaction', '', 'Client Satisfaction'),
(40, 'Our_Business_Model.php?mpid=30&mlid=32', 'Our Business Model', 1, 'Our Business Model', 'Our Business Model', '', 'Our Business Model'),
(41, 'Remote_Locations.php?mpid=30&mlid=33', 'Remote Locations', 1, 'Remote Locations', 'Remote Locations', '', 'Remote Locations'),
(42, 'Our_Clients.php?mpid=30&mlid=34', 'Our Clients', 1, 'Our Clients', 'Our Clients', '', 'Our Clients'),
(43, 'Installation.php?mpid=30&mlid=35', 'Installation', 1, 'Installation', 'Installation', '', 'Installation'),
(44, 'Core_Building_Products.php?mpid=4&mlid=20', 'Core Building Products', 1, 'Core Building Products with Procure it Direct', 'Core Building Products', '', 'Core Building Products'),
(45, 'Misc_Products.php?mpid=4&mlid=21', 'Misc Products', 1, 'Misc Products, Procure it Direct, Denzil Earland', 'Misc Products', '', 'Misc Products'),
(46, 'Consultancy.php?mpid=4&mlid=22', 'Consultancy', 1, 'Consultancy, by procure it direct UK', 'Consultancy', '', 'Consultancy'),
(47, 'Bespoke_Products.php?mpid=4&mlid=23', 'Bespoke Products', 1, 'Bespoke Products', 'Bespoke Products', '', 'Bespoke Products'),
(48, 'Sample_Rooms_&_Mockups.php?mpid=4&mlid=24', 'Sample Rooms & Mockups', 1, 'Sample Rooms & Mockups', 'Sample Rooms & Mockups', '', 'Sample Rooms & Mockups'),
(49, 'Branded_Products.php?mpid=4&mlid=25', 'Branded Products', 1, 'Branded Products', 'Branded Products', '', 'Branded Products'),
(50, 'Fixtures_fittings_&_equipment_with_procure_it_direct.php?mpid=5&mlid=36', 'Fixtures fittings & equipment with procure it direct', 1, 'Fixtures fittings & equipment with procure it direct', 'Fixtures fittings & equipment with procure it direct', '', 'Fixtures fittings & equipment with procure it direct'),
(51, 'Operations,_supplies_&_equipment.php?mpid=5&mlid=37', 'Operations, supplies & equipment', 1, 'Operations, supplies & equipment, with procure it direct', 'Operations, supplies & equipment, with procure it direct', '', 'Operations, supplies & equipment'),
(52, 'Retail.php?mpid=5&mlid=38', 'Retail', 1, 'Retail with procure it direct, we source it you save it', 'Retail with procure it direct, we source it you save it', '', 'Retail'),
(53, 'Office_Furniture.php?mpid=5&mlid=39', 'Office Furniture', 1, 'Office Furniture', 'Office Furniture', '', 'Office Furniture'),
(54, 'Developers.php?mpid=5&mlid=40', 'Developers', 1, 'Developers', 'Developers', '', 'Developers'),
(55, 'Education.php?mpid=5&mlid=41', 'Education', 1, 'Education procurement', 'Education procurement', '', 'Education'),
(56, 'Local_authorities_&_contractor_supply.php?mpid=5&mlid=42', 'Local authorities & contractor supply', 1, 'Local authorities & contractor supply, with global procurement,  from Procue it Direct , we source it you save it', 'Local authorities & contractor supply with global procurement from Procue it Direct we source it you save it', '', 'Local authorities & contractor supply'),
(57, 'Sustainable_procurement.php?mpid=5&mlid=43', 'Sustainable procurement', 1, 'Sustainable procurement, procure it direct', 'Sustainable procurement by Procure it Direct providing global procurement and fit-outs', '', 'Sustainable procurement'),
(58, 'Policies.php?mpid=6&mlid=44', 'Policies', 1, 'Policies', 'Policies', '', 'Policies'),
(59, 'People.php?mpid=6&mlid=45', 'People', 1, 'People', 'People', '', 'People'),
(60, 'Products.php?mpid=6&mlid=46', 'Products', 1, 'Products', 'Products', '', 'Products'),
(61, '1268996708.php?mpid=0&mlid=6', 'Environmental', 1, 'Environmental', 'Environmental', '', 'Environmental'),
(62, 'Czech_Republic.php?mpid=7&mlid=47', 'Czech Republic', 1, 'Czech Republic', 'Czech Republic', '', 'Czech Republic'),
(63, 'Procure_it_Direct_in_Dubai.php?mpid=7&mlid=48', 'Procure it Direct in Dubai', 1, 'Procure it Direct in Dubai', 'Procure it Direct in Dubai', '', 'Procure it Direct in Dubai'),
(64, 'Procure_it_Direct_in_Hong_Kong.php?mpid=7&mlid=49', 'Procure it Direct in Hong Kong', 1, 'Procure it Direct in Hong Kong', 'Procure it Direct in Hong Kong', '', 'Procure it Direct in Hong Kong'),
(65, 'Procure_it_Direct_in_China.php?mpid=7&mlid=50', 'Procure it Direct in China', 1, 'Procure it Direct in China', 'Procure it Direct in China ', '', 'Procure it Direct in China'),
(66, 'Procure_it_Direct_Kabul_Afghanistan_.php?mpid=7&mlid=51', 'Procure it Direct Kabul Afghanistan ', 1, 'Procure it Direct Kabul Afghanistan ', 'Procure it Direct Kabul Afghanistan ', '', 'Procure it Direct Kabul Afghanistan '),
(67, 'Procure_it_Direct_in_Turkmenistan.php?mpid=7&mlid=52', 'Procure it Direct in Turkmenistan', 1, 'Procure it Direct in Turkmenistan', 'Procure it Direct in Turkmenistan', '', 'Procure it Direct in Turkmenistan'),
(68, 'Procure_it_Direct_in_London_UK.php?mpid=7&mlid=53', 'Procure it Direct in London UK', 1, 'Procure it Direct in London UK', 'Procure it Direct in London UK', '', 'Procure it Direct in London UK'),
(69, 'Procure_it_Direct_in_Qatar.php?mpid=7&mlid=54', 'Procure it Direct in Qatar', 1, 'Procure it Direct in Qatar', 'Procure it Direct in Qatar', '', 'Procure it Direct in Qatar'),
(70, 'Contact_Details_for_Procure_it_Direct.php?mpid=0&mlid=7', 'Contact Details for Procure it Direct', 1, 'Contact Details for Procure it Direct', 'Contact Details for Procure it Direct', '', 'Contact Details for Procure it Direct'),
(71, 'NEW_ONE.php?mpid=6&mlid=55', 'NEW ONE', 1, 'NEW ONE', 'NEW ONE', '', 'NEW ONE'),
(72, 'E-Shop.php?mpid=0&mlid=56', 'E-Shop', 2, 'E-Shop', 'E-Shop', '', 'E-Shop'),
(73, 'Cardiff_Seminar.php?mpid=8&mlid=57', 'Cardiff Seminar', 4, 'PiD representatives held a very sucessful seminar in Cardiff explaining the advantages of our services. Click below to see the presentation\r\nhttp://www.youtube.com/watch?v=JT7ziIAy2G4\r\n', 'PiD representatives held a very sucessful seminar in Cardiff explaining the advantages of our services. Click below to see the presentation\r\nhttp://www.youtube.com/watch?v=JT7ziIAy2G4\r\n', '', 'Cardiff Seminar'),
(74, 'Caribbean.php?mpid=7&mlid=58', 'Caribbean', 1, 'Caribbean', 'Caribbean', '', 'Caribbean');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `users_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` varchar(20) DEFAULT NULL,
  `userpass` varchar(255) NOT NULL DEFAULT '',
  `firstname` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `email` varchar(60) DEFAULT NULL,
  `mobile` varchar(60) DEFAULT NULL,
  `security_level` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`users_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`users_id`, `userid`, `userpass`, `firstname`, `lastname`, `email`, `mobile`, `security_level`) VALUES
(1, 'admin', '1359e39dc07ed5768adfe1b1209b8e55', 'Administrator', 'mylastname', 'fred@inwales.com', '', ''),
(2, 'user1', '936d2afa5689483939e4efff48f42ab5', 'Administrator', 'mylastname', 'fred@inwales.com', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `wide_images`
--

CREATE TABLE IF NOT EXISTS `wide_images` (
  `image_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `image_page_id` int(10) unsigned DEFAULT NULL,
  `image_name` varchar(30) DEFAULT '',
  `image_alt_tag` varchar(150) DEFAULT '',
  PRIMARY KEY (`image_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `wide_images`
--

INSERT INTO `wide_images` (`image_id`, `image_page_id`, `image_name`, `image_alt_tag`) VALUES
(20, 21, '1268908590.jpg', ''),
(21, 23, '1268909132.jpg', 'E Catalogue'),
(22, 27, '1268942331.jpg', ''),
(23, 35, '1269003731.jpg', ''),
(24, 72, '1269519095.jpg', '');
